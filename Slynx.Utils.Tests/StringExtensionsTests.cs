﻿// 
// StringExtensionsTests.cs is a part of Slynx.Utils.Tests project.
// 
// Created by Slynx on 10/05/2020 23:13.

using System;
using Slynx.Utils.ExtensionMethods;
using Xunit;

namespace Slynx.Utils.Tests
{
    public class StringExtensionsTests
    {
        [Theory]
        [InlineData("asd123", 8, "asd123")]
        [InlineData("asd123", 3, "asd")]
        [InlineData("asd123", -1, "")]
        public void Left(String input, Int32 count, String expected)
        {
            Assert.Equal(expected, input.Left(count));
        }

        [Theory]
        [InlineData("asd123", 8, "asd123")]
        [InlineData("asd123", 4, "d123")]
        [InlineData("asd123", -1, "")]
        public void Right(String input, Int32 count, String expected)
        {
            Assert.Equal(expected, input.Right(count));
        }

        [Theory]
        [InlineData("asd123", 10, "  asd123  ")]
        public void PadCenter(String input, Int32 count, String expected)
        {
            Assert.Equal(expected, input.PadCenter(count, ' '));
        }

        [Theory]
        [InlineData("asd123", "asd", "123")]
        [InlineData("asd123", "123", "asd123")]
        public void TrimPrefix(String input, String prefix, String expected)
        {
            Assert.Equal(expected, input.TrimPrefix(prefix));
        }

        [Theory]
        [InlineData("asd123", "123", "asd")]
        [InlineData("asd123", "asd", "asd123")]
        public void TrimSuffix(String input, String prefix, String expected)
        {
            Assert.Equal(expected, input.TrimSuffix(prefix));
        }

        [Theory]
        [InlineData("abc.def.ghe", "abc", ".ghe", false, ".def")]
        [InlineData("abc.def.ghe", "c", ".gh", true, "c.def.gh")]
        [InlineData("abc.def.ghe", "abc", null, false, ".def.ghe")]
        [InlineData("abc.def.ghe", null, null, false, "abc.def.ghe")]
        [InlineData("abc.def.ghe", null, null, true, "abc.def.ghe")]
        [InlineData("abc.def.ghe", null, "f.g", true, "abc.def.g")]
        public void FindBetween(String input, String begin, String end, Boolean includeStartEnd, String expected)
        {
            Assert.Equal(expected, input.FindBetween(begin, end, includeStartEnd));
        }

        [Theory]
        [InlineData("s1s2s3s4", new[] { "s1", "s3", "m" }, "ms2ms4")]
        public void ReplaceWithLast(String input, String[] pars, String expected)
        {
            Assert.Equal(expected, input.ReplaceWithLast(pars));
        }

        [Theory]
        [InlineData("s1s2s3s4", new[] { "s1", "m", "s3", "b" }, "ms2bs4")]
        public void ReplaceByPair(String input, String[] pars, String expected)
        {
            Assert.Equal(expected, input.ReplaceByPair(pars));
        }
    }
}