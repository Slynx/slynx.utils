﻿// 
// CRC32Simple.cs is a part of Slynx.Utils.Tests project.
// 
// Created by Slynx on 10/05/2020 23:13.

using System;
using System.IO;

namespace Slynx.Utils.Tests
{
    // Calculate crc using standard, well-known method
    internal class CRC32Simple
    {
        private static readonly UInt32[] crc32Table = new UInt32[256];
        private UInt32 crc32Result = 0xFFFFFFFF;

        static CRC32Simple()
        {
            var polynomial = 0xEDB88320;
            for (UInt32 i = 0; i < 256; i++)
            {
                var crc32 = i;
                for (var j = 8; j > 0; j--)
                {
                    if ((crc32 & 1) == 1)
                    {
                        crc32 = (crc32 >> 1) ^ polynomial;
                    }
                    else
                    {
                        crc32 >>= 1;
                    }
                }

                crc32Table[i] = crc32;
            }
        }

        public UInt32 Compute(Byte[] array, Int32 start, Int32 size)
        {
            crc32Result = 0xFFFFFFFF;
            var end = start + size;
            for (var i = start; i < end; i++)
            {
                crc32Result = (crc32Result >> 8) ^ crc32Table[array[i] ^ (crc32Result & 0x000000FF)];
            }

            return ~crc32Result;
        }

        public UInt32 Compute(Stream stream)
        {
            crc32Result = 0xFFFFFFFF;
            var buffer = new Byte[10 << 20];
            var read = -1;
            while ((read = stream.Read(buffer, 0, buffer.Length)) > 0)
            {
                for (var i = 0; i < read; i++)
                {
                    crc32Result = (crc32Result >> 8) ^ crc32Table[buffer[i] ^ (crc32Result & 0x000000FF)];
                }
            }

            return ~crc32Result;
        }
    }
}