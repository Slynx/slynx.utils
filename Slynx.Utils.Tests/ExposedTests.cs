﻿// 
// ExposedTests.cs is a part of Slynx.Utils.Tests project.
// 
// Created by Slynx on 31/12/2020 11:27.

using System;
using System.Collections.Generic;
using Slynx.Utils.ExtensionMethods;
using Xunit;

namespace Slynx.Utils.Tests
{
    public class ExposedTests
    {
        [Fact]
        public void GetMember()
        {
            dynamic test = new TestClass().AsExposed();
            Assert.Equal("Property1", (String)test.Property1);
            Assert.Equal("Property2", (String)test.Property2);
            Assert.Equal("field", (String)test.field);
            Assert.Equal("Test1", (String)test.PropertyTest.Test1);
            Assert.Equal("internalField", (String)test.internalField);
            Assert.Null((String)test.PropertyTest2?.Test1);
        }

        [Fact]
        public void SetMember()
        {
            dynamic test = new TestClass().AsExposed();
            test.Property2 = "asd";
            test.PropertyTest.Test1 = "bbb";
            test.PropertyTest2 = test.PropertyTest;
            test.internalField = "newVal";
            Assert.Equal("asd", (String)test.Property2);
            Assert.Equal("bbb", (String)test.PropertyTest.Test1);
            Assert.Equal(test.PropertyTest.ToObject(), test.PropertyTest2.ToObject());
            Assert.Equal("newVal", (String)test.internalField);
            Assert.Equal("newVal", test.internalField.Cast<String>());
            Assert.Throws<InvalidCastException>(() => test.internalField.Cast<Int32>());

            var testParent = new TestClassParent().AsExposed();
            testParent.internalField = "newVal";
            Assert.Equal("newVal", (String)testParent.internalField);
        }

        [Fact]
        public void StaticTest()
        {
            dynamic test = typeof(TestClass).AsExposed();
            Assert.Equal("123", (String)test.CallMe(123));
            test.StaticProperty = "123";
            Assert.Equal("123", (String)test.StaticProperty);
        }

        [Fact]
        public void TryGetSetIndex()
        {
            dynamic test = new TestClass().AsExposed();
            test[6] = "6";
            Assert.Equal("6", (String)test[6]);
            test["4", 4] = 123;
            Assert.Equal(123, (Int32)test["4", 4]);
            var g = Guid.NewGuid();
            test["4", g] = 125;
            Assert.Equal(125, (Int32)test["4", g]);
        }

        [Fact]
        public void TryInvokeMember()
        {
            dynamic test = new TestClass().AsExposed();
            test.Method1();
            Assert.Equal("inp", (String)test.Method2("inp"));
            Assert.Equal("str", (String)test.ToString());
            Assert.True((Boolean)test.Met1(true));

            var i = 5;
            test.Method3(ref i);
            Assert.Equal(6, i);
        }
#pragma warning disable 414

        private class TestClassParent : TestClass
        {
        }

        private class TestClass
        {
            private class TestClass2
            {
                private String Test1 { get; set; } = "Test1";
            }

            private readonly Dictionary<Int32, String> indexer = new Dictionary<Int32, String>();
            private readonly Dictionary<String, Int32> indexer2 = new Dictionary<String, Int32>();

            private String field = "field";

            internal String internalField = nameof(internalField);

            private String Property1 { get; } = "Property1";

            private String Property2 { get; set; } = "Property2";

            private TestClass2 PropertyTest { get; } = new TestClass2();

            private TestClass2 PropertyTest2 { get; set; }

            private String this[Int32 num]
            {
                get { return indexer[num]; }
                set { indexer[num] = value; }
            }

            private Int32 this[String num, Int32 val]
            {
                get { return indexer2[num + val]; }
                set { indexer2[num + val] = value; }
            }

            private Int32 this[String num, Guid val]
            {
                get { return indexer2[num + val]; }
                set { indexer2[num + val] = value; }
            }

            internal static String StaticProperty { get; set; }

            private void Method1()
            {
            }

            private String Method2(String input)
            {
                return input;
            }

            private void Method3(ref Int32 nr)
            {
                nr = 6;
            }

            public override String ToString()
            {
                return "str";
            }

            internal static String CallMe(Int32 nr)
            {
                return nr.ToString();
            }

            internal Boolean Met1(Boolean result)
            {
                return result;
            }
        }
#pragma warning restore 414
    }
}