﻿// 
// EnumerableTests.cs is a part of Slynx.Utils.Tests project.
// 
// Created by Slynx on 07/07/2021 11:36.

using System;
using Slynx.Utils.ExtensionMethods;
using Xunit;

namespace Slynx.Utils.Tests
{
    public class EnumerableTests
    {
        [Fact]
        public void DistinctBy()
        {
            var items = new Int32[] { 1, 2, 4, 2, 1 };
            Assert.Equal(new[] { 1, 2, 4 }, items.DistinctBy(s => s));
        }
    }
}