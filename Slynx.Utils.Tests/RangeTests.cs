﻿// 
// RangeTests.cs is a part of Slynx.Utils.Tests project.
// 
// Created by Slynx on 14/06/2020 00:59.

using System;
using Xunit;

namespace Slynx.Utils.Tests
{
    public class RangeTests
    {
        [Theory]
        [InlineData(-4, -1, -3)]
        public void Left(Decimal min, Decimal max, Decimal value)
        {
            var r = new Range<Decimal>(min, max);
            Assert.True(r.ContainsValue(value));
        }
    }
}