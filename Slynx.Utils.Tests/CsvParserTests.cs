﻿// 
// CsvParserTests.cs is a part of Slynx.Utils.Tests project.
// 
// Created by Slynx on 04/02/2021 12:39.

using System;
using System.IO;
using System.Text;
using Slynx.Utils.Core.Parsers.Csv;
using Xunit;

namespace Slynx.Utils.Tests
{
    public class CsvParserTests
    {
        private const String TestInput1 = "val1;val 2;15;55.4";
        private const String TestInput2 = "Header1;Header 2;Header3;Header4\nval1;val 2;15;55.4";
        private const String TestInput4 = "Header1;Header 2;Header3;Header4\nval1;val 2;55.4;15";
        private const String TestInput3 = "h1\nv1\nv2\nv3\nv4";

        public class RecordRead
        {
            [CsvColumn("Header1", 0)]
            public String Value1 { get; set; }

            [CsvColumn("Header 2", 1)]
            public String Value2 { get; set; }

            [CsvColumn("Header3", 2, Format = "0.000")]
            public Double NrDouble { get; set; }

            [CsvColumn("Header4", -1)]
            public Int32 NrInt { get; set; }
        }

        [Theory]
        [InlineData(TestInput4)]
        public void TestReaderToObject(String input)
        {
            using (var stream = new MemoryStream(Encoding.UTF8.GetBytes(input)))
            {
                using (var reader = new CsvReader(stream) { Delimiter = ';', Header = HeaderBehavior.Ignore })
                {
                    foreach (var row in reader)
                    {
                        var result = row.Get<RecordRead>();
                        Assert.Equal("val1", result.Value1);
                        Assert.Equal("val 2", result.Value2);
                        Assert.Equal(15, result.NrInt);
                        Assert.Equal(55.4, result.NrDouble);
                    }
                }
            }
        }

        [Theory]
        [InlineData(TestInput1)]
        public void TestReader(String input)
        {
            using (var stream = new MemoryStream(Encoding.UTF8.GetBytes(input)))
            {
                using (var reader = new CsvReader(stream) { Delimiter = ';' })
                {
                    foreach (var row in reader)
                    {
                        Assert.Equal("val1", row.Get<String>(0));
                        Assert.Equal("val 2", row.Get<String>(1));
                        Assert.Equal(15, row.Get<Int32>(2));
                        Assert.Equal(55.4d, row.Get<Double>(3));
                    }
                }
            }
        }

        [Theory]
        [InlineData(TestInput3)]
        public void TestReaderLineNumber(String input)
        {
            using (var stream = new MemoryStream(Encoding.UTF8.GetBytes(input)))
            {
                using (var reader = new CsvReader(stream) { Delimiter = ';', Header = HeaderBehavior.Ignore })
                {
                    var counter = 1; // start from zero (header)
                    foreach (CsvReaderRow row in reader)
                    {
                        Assert.Equal(counter++, row.LineNumber);
                    }
                }
            }
        }

        [Theory]
        [InlineData("\"asd\", \"5,76\"", "asd", "5,76")]
        [InlineData("\"asd\", 5.76", "asd", "5.76")]
        public void TestReaderWithQuoute(params String[] input)
        {
            using (var stream = new MemoryStream(Encoding.UTF8.GetBytes(input[0])))
            {
                using (var reader = new CsvReader(stream) { Delimiter = ',', Quote = '"' })
                {
                    foreach (var row in reader)
                    {
                        for (var i = 1; i < input.Length; i++)
                        {
                            Assert.Equal(input[i], row.Get<String>(i - 1));
                        }
                    }
                }
            }
        }

        [Theory]
        [InlineData(TestInput2)]
        public void TestReaderWithHeader(String input)
        {
            using (var stream = new MemoryStream(Encoding.UTF8.GetBytes(input)))
            {
                using (var reader = new CsvReader(stream) { Delimiter = ';', Header = HeaderBehavior.Ignore })
                {
                    foreach (var row in reader)
                    {
                        Assert.True(row.Get<String>(0) == "val1" && row.Get<String>(1) == "val 2" && row.Get<Int32>(2) == 15 && row.Get<Double>(3) == 55.4d);
                    }
                }
            }
        }

        [Theory]
        [InlineData(TestInput2)]
        public void TestReaderWithHeaderAsFirstRecord(String input)
        {
            using (var stream = new MemoryStream(Encoding.UTF8.GetBytes(input)))
            {
                using (var reader = new CsvReader(stream) { Delimiter = ';', Header = HeaderBehavior.AsFirstRecord })
                {
                    foreach (CsvReaderRow row in reader)
                    {
                        if (row.IsHeader)
                        {
                            continue;
                        }

                        Assert.True(row.Get<String>(0) == "val1" && row.Get<String>(1) == "val 2" && row.Get<Int32>(2) == 15 && row.Get<Double>(3) == 55.4d);
                    }
                }
            }
        }

        [Theory]
        [InlineData("\"val1\",\"val 2\",\"23\",\"4.5\"")]
        public void TestWriterWithQuote(String input)
        {
            using (var stream = new MemoryStream())
            {
                using (var writer = new CsvWriter(stream) { Quote = '"', Delimiter = ',' })
                {
                    writer.WriteRow(input.Replace("\"", String.Empty).Split(','));
                }

                Assert.Equal(input, Encoding.UTF8.GetString(stream.ToArray()).TrimEnd('\r', '\n'), ignoreLineEndingDifferences: true);
            }
        }

        [Theory]
        [InlineData("val1,val 2,23,4.5")]
        public void TestWriter(String input)
        {
            using (var stream = new MemoryStream())
            {
                using (var writer = new CsvWriter(stream) { Quote = null, Delimiter = ',' })
                {
                    writer.WriteRow(input.Split(','));
                }

                Assert.Equal(input, Encoding.UTF8.GetString(stream.ToArray()).TrimEnd('\r', '\n'), ignoreLineEndingDifferences: true);
            }
        }

        public class Record
        {
            [CsvColumn("Header1", 0)]
            public String Value1 { get; set; }

            [CsvColumn("Header 2", 1)]
            public String Value2 { get; set; }

            [CsvColumn("Header3", 2, Format = "0.000")]
            public Double NrDouble { get; set; }

            [CsvColumn("Header4", 3)]
            public Int32 NrInt { get; set; }
        }

        [Fact]
        public void TestWriterWithAttributes()
        {
            using (var stream = new MemoryStream())
            {
                using (var writer = new CsvWriter(stream) { Quote = null, Delimiter = ',', IgnoreHeader = true })
                {
                    var record = new Record();
                    record.Value1 = "val1";
                    record.Value2 = "val 2";
                    record.NrDouble = 4.5d;
                    record.NrInt = 23;
                    writer.WriteRow(record);
                }

                Assert.Equal("val1,val 2,4.500,23", Encoding.UTF8.GetString(stream.ToArray()).TrimEnd('\r', '\n'), ignoreLineEndingDifferences: true);
            }
        }

        [Fact]
        public void TestWriterWithAttributesAndHeader()
        {
            using (var stream = new MemoryStream())
            {
                using (var writer = new CsvWriter(stream) { Quote = null, Delimiter = ',', IgnoreHeader = false })
                {
                    var record = new Record();
                    record.Value1 = "val1";
                    record.Value2 = "val 2";
                    record.NrDouble = 4.5d;
                    record.NrInt = 23;
                    writer.WriteRow(record);
                }

                Assert.Equal("Header1,Header 2,Header3,Header4\r\nval1,val 2,4.500,23", Encoding.UTF8.GetString(stream.ToArray()).TrimEnd('\r', '\n'), ignoreLineEndingDifferences: true);
            }
        }
    }
}