﻿// 
// RangeTests.cs is a part of Slynx.Utils.Tests project.
// 
// Created by Slynx on 14/06/2020 00:59.

using System;
using System.Linq;
using Slynx.Utils.Core;
using Slynx.Utils.ExtensionMethods;
using Xunit;

namespace Slynx.Utils.Tests
{
    public class ArrayExtensionsTests
    {
        [Theory]
        [InlineData(5, -1)]
        [InlineData(5, 10)]
        [InlineData(5, 2)]
        [InlineData(5, 0)]
        [InlineData(5, 5)]
        public void Insert(Int32 length, Int32 index)
        {
            var array = Enumerable.Range(0, length).ToArray();
            array = array.Insert(Int32.MaxValue, index);
            var testIndex = MathUtils.Clamp(index, 0, length);
            Assert.True(array.Length - 1 >= testIndex);
            Assert.True(array[testIndex] == Int32.MaxValue);
            var counter = 0;
            for (var i = 0; i < array.Length; i++)
            {
                if (i == testIndex)
                {
                    continue;
                }
                Assert.True(array[i] == counter);
                counter++;
            }
        }
    }
}