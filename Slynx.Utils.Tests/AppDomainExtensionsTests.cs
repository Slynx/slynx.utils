// 
// AppDomainExtensionsTests.cs is a part of Slynx.Utils.Tests project.
// 
// Created by Slynx on 10/05/2020 23:13.

using System;
using Slynx.Utils.ExtensionMethods;
using Xunit;

namespace Slynx.Utils.Tests
{
    public class AppDomainExtensionsTests
    {
        [Theory]
        [InlineData("System.Data")]
        public void AddAssemblyResolvePath_ShouldResolve(String asm)
        {
            AppDomain.CurrentDomain.AddAssemblyResolvePath(() => String.Empty);
            AppDomain.CurrentDomain.Load(asm);
        }
    }
}