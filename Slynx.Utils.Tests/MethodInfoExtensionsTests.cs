﻿// 
// MethodInfoExtensionsTests.cs is a part of Slynx.Utils.Tests project.
// 
// Created by Slynx on 10/05/2020 23:13.

using System;
using System.Reflection;
using Slynx.Utils.ExtensionMethods;
using Xunit;

namespace Slynx.Utils.Tests
{
    public class MethodInfoExtensionsTests
    {
        private class TestBaseClass
        {
            public virtual Boolean VirtualProperty { get; set; }

            public virtual Boolean VirtualPropertyOverriden { get; set; }

            public Boolean NonVirtualProperty { get; set; }

            public Boolean VoidBaseCall { get; set; }

            public Boolean VoidBaseCall2 { get; set; }

            public virtual Boolean VirtualMethod(TestBaseClass s)
            {
                InternalCall(s);
                return true;
            }

            protected virtual void InternalCall(TestBaseClass par)
            {
            }

            public virtual Boolean VirtualMethodOverriden()
            {
                return true;
            }

            public Boolean NonVirtualMethod()
            {
                return true;
            }

            public virtual String TestString()
            {
                return "Base";
            }

            public virtual void TestVoid()
            {
                VoidBaseCall = true;
            }

            public virtual void TestVoidParams(String s1, String s2)
            {
                VoidBaseCall2 = true;
            }

            public virtual String TestStringParams(String s1, String s2, String s3)
            {
                return "SRT";
            }

            public virtual String TestRef(String s1, ref String s2)
            {
                s2 = "1";
                return "123";
            }
        }

        private class TestDerivedClass : TestBaseClass
        {
            public override Boolean VirtualPropertyOverriden { get; set; }

            public override Boolean VirtualMethodOverriden()
            {
                return false;
            }

            public override String TestString()
            {
                return "Der";
            }

            public override void TestVoid()
            {
                VoidBaseCall = false;
            }

            public override void TestVoidParams(String s1, String s2)
            {
                VoidBaseCall2 = false;
            }

            public override String TestStringParams(String s1, String s2, String s3)
            {
                return "XXX";
            }
        }

        [Fact]
        public void InvokeNotOverriden()
        {
            var derived = new TestDerivedClass();

            var method = typeof(TestBaseClass).GetMethod(nameof(TestBaseClass.TestString), BindingFlags.Instance | BindingFlags.Public);
            Assert.Equal("Base", method.InvokeNotOverriden<String>(derived));

            var method2 = typeof(TestBaseClass).GetMethod(nameof(TestBaseClass.TestVoid), BindingFlags.Instance | BindingFlags.Public);
            method2.InvokeNotOverriden(derived);
            Assert.True(derived.VoidBaseCall);

            var method3 = typeof(TestBaseClass).GetMethod(nameof(TestBaseClass.TestVoidParams), BindingFlags.Instance | BindingFlags.Public);
            method3.InvokeNotOverriden(derived, "S1", "S2");
            Assert.True(derived.VoidBaseCall2);

            var method4 = typeof(TestBaseClass).GetMethod(nameof(TestBaseClass.TestStringParams), BindingFlags.Instance | BindingFlags.Public);
            Assert.Equal("SRT", method4.InvokeNotOverriden<String>(derived, "S1", "S2"));

            var method5 = typeof(TestBaseClass).GetMethod(nameof(TestBaseClass.TestRef), BindingFlags.Instance | BindingFlags.Public);
            Assert.Throws<NotSupportedException>(() => method5.InvokeNotOverriden<String>(derived, "S1", "123"));

            var method6 = typeof(TestBaseClass).GetMethod(nameof(TestBaseClass.VirtualMethod), BindingFlags.Instance | BindingFlags.Public);
            method6.InvokeNotOverriden(derived, derived);
        }

        [Fact]
        public void IsMethodOverriden()
        {
            var type = typeof(TestDerivedClass);
            Assert.False(type.GetMethod(nameof(TestBaseClass.VirtualMethod)).IsOverridden());
            Assert.True(type.GetMethod(nameof(TestBaseClass.VirtualMethodOverriden)).IsOverridden());
            Assert.False(type.GetMethod(nameof(TestBaseClass.NonVirtualMethod)).IsOverridden());

            Assert.False(type.GetMethod("get_" + nameof(TestBaseClass.VirtualProperty)).IsOverridden());
            Assert.True(type.GetMethod("get_" + nameof(TestBaseClass.VirtualPropertyOverriden)).IsOverridden());
            Assert.False(type.GetMethod("get_" + nameof(TestBaseClass.NonVirtualProperty)).IsOverridden());
        }
    }
}