﻿// 
// TreeTests.cs is a part of Slynx.Utils.Tests project.
// 
// Created by Slynx on 10/05/2020 23:13.

using System;
using Slynx.Utils.Core.Collections;
using Xunit;

namespace Slynx.Utils.Tests
{
    public class TreeTests
    {
        [Fact]
        public void Add()
        {
            var tree = new UniqueTree<String, Int32>();
            tree.IsReadOnly = false;
            tree.Add("sds", 0);
            tree.Add("sd", 2);
            tree["1"]["2"]["3"].Value = 7;
            Assert.Equal(7, tree["1"]["2"]["3"].Value);
        }

        [Fact]
        public void DynamicAdd()
        {
            var tree = new UniqueTree<String, String> { IsReadOnly = false };
            tree["1"]["2"]["3"].Value = "val123";
            Assert.Equal("val123", tree["1"]["2"]["3"].Value);
        }

        [Fact]
        public void SealedError()
        {
            var tree = new UniqueTree<String, String> { IsReadOnly = true };
            Assert.Throws<InvalidOperationException>(() => tree["1"]["2"]["3"].Value = "val123");
        }
    }
}