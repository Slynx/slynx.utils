﻿// 
// FormattersTests.cs is a part of Slynx.Utils.Tests project.
// 
// Created by Slynx on 08/09/2021 15:55.

using System;
using System.Globalization;
using Xunit;

namespace Slynx.Utils.Tests
{
    public class FormattersTests
    {
        [Fact]
        public void SlynxFormatter()
        {
            IFormatProvider formatter = new SlynxFormatProvider();
            //Assert.Equal("", 122.54.ToString("FS", formatter));
            Assert.Equal($"{1.78.ToString(CultureInfo.CurrentCulture)} GB", String.Format(formatter, "{0:FS}", 1907341823));
            Assert.Equal($"{120.54.ToString(CultureInfo.CurrentCulture)} KB-{4.35.ToString(CultureInfo.CurrentCulture)} MB", String.Format(formatter, "{0:FSR}", new Range<Double>(123432, 4563565)));
            Assert.Equal($"{3.09.ToString(CultureInfo.CurrentCulture)} MB/s", String.Format(formatter, "{0:SPEED}", 3242342));
            Assert.Equal("00:38:54", String.Format(formatter, "{0:TIME}", 2334));
        }
    }
}