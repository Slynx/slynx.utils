﻿// 
// CRC32HashAlgorithmTests.cs is a part of Slynx.Utils.Tests project.
// 
// Created by Slynx on 10/05/2020 23:13.

using System;
using System.IO;
using System.Security.Cryptography;
using Slynx.Utils.Core.Debugging;
using Slynx.Utils.Core.Security.Cryptography;
using Xunit;
using Xunit.Abstractions;

namespace Slynx.Utils.Tests
{
    public class CRC32HashAlgorithmTests : IDisposable
    {
        public CRC32HashAlgorithmTests(ITestOutputHelper testOutputHelper)
        {
            this.testOutputHelper = testOutputHelper;
        }


        public void Dispose()
        {
            data = null;
            sampleCrc = 0;
        }

        private readonly ITestOutputHelper testOutputHelper;
        private Byte[] data;
        private UInt32 sampleCrc;

        private void CreateSample(Int32 sampleLength = 10 << 20)
        {
            data = new Byte[sampleLength];
            var rand = new Random();
            rand.NextBytes(data);

            var c = new CRC32Simple();
            sampleCrc = c.Compute(data, 0, data.Length);
        }

        private void OnProgressChanged(Object sender, PercentageProgressChangedEventArgs e)
        {
            testOutputHelper.WriteLine($"Progress: {e.Value}%");
        }

        [Fact]
        public void TestHashAlgorithm()
        {
            CreateSample(100 << 20);
            Assert.NotNull(data);

            HashAlgorithm cal = new Crc32();

            testOutputHelper.WriteLine(Timer.Measure(() =>
            {
                var b = cal.ComputeHash(data, 0, data.Length);
                Assert.NotNull(data);
                Assert.True(b.Length > 0);
                Assert.True(BitConverter.ToUInt32(b, 0) == sampleCrc);

                Assert.Equal(b, ((Crc32)cal).Format<Byte[]>());
                Assert.True(cal.HashSize == 32);
            }));
        }

        /*[Fact]
        public void TestIHashAlgorithm_MultipleFiles()
        {
            var s = new CRC32Simple();
            var cal = new Crc32();
            foreach (string file in Directory.GetFiles(@"files"))
            {
                using (var stream = File.OpenRead(file))
                {
                    var expected = s.Compute(stream);
                    stream.Position = 0;
                    cal.ComputeMultiCore(stream);
                    var result = cal.Format<UInt32>();
                    Assert.Equal(expected, result);
                }
            }
        }*/

        [Fact]
        public void TestIHashAlgorithm_MultiCore()
        {
            CreateSample(1024 << 20);

            Assert.NotNull(data);
            var cal = new Crc32();

            cal.ProgressChanged += OnProgressChanged;

            testOutputHelper.WriteLine(Timer.Measure(() =>
            {
                using (var str = new MemoryStream(data))
                {
                    cal.ComputeMultiCore(str);
                }

                var crc = cal.Format<UInt32>();
                testOutputHelper.WriteLine("CRC: " + cal.Format<String>());
                Assert.Equal(crc, sampleCrc);
            }, 4));
        }

        [Fact]
        public void TestIHashAlgorithm_Normal()
        {
            CreateSample(1024 << 20);

            Assert.NotNull(data);
            var cal = new Crc32();
            cal.ProgressChanged += OnProgressChanged;

            testOutputHelper.WriteLine(Timer.Measure(() =>
            {
                cal.Compute(data);
                var crc = cal.Format<UInt32>();
                Assert.Equal(crc, sampleCrc);
            }, 4));
        }

        /*[Fact]
        public void TestMultiCoreHugeFile()
        {
            // This required huge file on disk to test memory consumption.
            var file = @"file";
            var cal = new Crc32();
            cal.ProgressChanged += OnProgressChanged;
            using (var str = File.OpenRead(file))
            {
                testOutputHelper.WriteLine(Timer.Measure(() => { cal.ComputeMultiCore(str); }));
            }

            var s = new CRC32Simple();
            using (var str = File.OpenRead(file))
            {
                UInt32 result = 0;
                testOutputHelper.WriteLine(Timer.Measure(() => { result = s.Compute(str); }));
                Assert.Equal(result, cal.Format<UInt32>());
            }
        }*/
    }
}