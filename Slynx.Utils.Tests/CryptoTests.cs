﻿// 
// CryptoTests.cs is a part of Slynx.Utils.Tests project.
// 
// Created by Slynx on 23/03/2021 02:24.

using System;
using Slynx.Utils.Core.Security.Cryptography;
using Xunit;

namespace Slynx.Utils.Tests
{
    public class CryptoTests
    {
        [Theory]
        [InlineData("inp", "k2")]
        public void Test(String plain, String key)
        {
            Assert.Equal(plain, Crypto.Decrypt(Crypto.Encrypt(plain, key), key));
        }
    }
}