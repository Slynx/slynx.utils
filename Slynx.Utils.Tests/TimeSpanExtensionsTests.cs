﻿// 
// TimeSpanExtensionsTests.cs is a part of Slynx.Utils.Tests project.
// 
// Created by Slynx on 23/12/2020 01:02.

using System;
using System.Globalization;
using Slynx.Utils.ExtensionMethods;
using Xunit;

namespace Slynx.Utils.Tests
{
    public class TimeSpanExtensionsTests
    {
        [Theory]
        [InlineData(10, 5, 12, 5, "pl-PL", "10 dni, 5 godzin, 12 minut i 5 sekund")]
        [InlineData(0, 0, 12, 5, "pl-PL", "12 minut i 5 sekund")]
        [InlineData(0, 0, 0, 1, "pl-PL", "1 sekunda")]
        [InlineData(1000, 0, 0, 1, "pl-PL", "2 lata, 8 miesięcy, 25 dni i 1 sekunda")]
        [InlineData(364, 0, 0, 0, "pl-PL", "11 miesięcy i 29 dni")]
        [InlineData(10, 5, 12, 5, "en-US", "10 days, 5 hours, 12 minutes and 5 seconds")]
        [InlineData(0, 0, 12, 5, "en-US", "12 minutes and 5 seconds")]
        [InlineData(0, 0, 0, 1, "en-US", "1 second")]
        [InlineData(1000, 0, 0, 1, "en-US", "2 years, 8 months, 25 days and 1 second")]
        [InlineData(364, 0, 0, 0, "en-US", "11 months and 29 days")]
        public void ToUserFriendlyString(Int32 days, Int32 hours, Int32 minutes, Int32 seconds, String cultureInfo, String result)
        {
            var span = new TimeSpan(days, hours, minutes, seconds);
            var us = span.ToUserFriendlyString(CultureInfo.GetCultureInfo(cultureInfo));
            Assert.Equal(result, us);
        }
    }
}