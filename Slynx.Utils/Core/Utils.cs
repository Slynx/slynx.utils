﻿// 
// Utils.cs is a part of Slynx.Utils project.
// 
// Created by Slynx on 10/05/2020 23:13.

using System;

namespace Slynx.Utils.Core
{
    /// <summary>
    ///     Class with some static support methods.
    /// </summary>
    public static class Utils
    {
        /// <summary>
        ///     Calculates percentage (0-100) progress from <paramref name="part" /> and <paramref name="total" />
        /// </summary>
        /// <param name="part"></param>
        /// <param name="total"></param>
        /// <returns></returns>
        public static Int32 CalculatePercentageProgress(Decimal part, Decimal total)
        {
            return Decimal.ToInt32(Decimal.Divide(part, total) * 100);
        }

        /// <summary>
        ///     Calculates percentage (0-100) progress from <paramref name="part" /> and <paramref name="total" />
        /// </summary>
        /// <param name="part"></param>
        /// <param name="total"></param>
        /// <returns></returns>
        public static Int32 CalculatePercentageProgress(Double part, Double total)
        {
            return (Int32)((part / total) * 100);
        }

        /// <summary>
        ///     Calculates percentage (0-100) progress from <paramref name="part" /> and <paramref name="total" />
        /// </summary>
        /// <param name="part"></param>
        /// <param name="total"></param>
        /// <returns></returns>
        public static Int32 CalculatePercentageProgress(Int64 part, Int64 total)
        {
            return (Int32)(((Double)part / total) * 100);
        }
    }
}