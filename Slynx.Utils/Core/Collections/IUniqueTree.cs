﻿// 
// IUniqueTree.cs is a part of Slynx.Utils project.
// 
// Created by Slynx on 10/05/2020 23:13.

using System;

namespace Slynx.Utils.Core.Collections
{
    public interface IUniqueTree<TKey, TValue> : IUniqueTreeNode<TKey, TValue>
    {
        Boolean IsReadOnly { get; set; }
    }
}