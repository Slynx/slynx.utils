﻿// 
// UniqueTree.cs is a part of Slynx.Utils project.
// 
// Created by Slynx on 10/05/2020 23:13.

using System;

namespace Slynx.Utils.Core.Collections
{
    public class UniqueTree<TKey, TValue> : UniqueTreeNode<TKey, TValue>, IUniqueTree<TKey, TValue>
    {
        public UniqueTree() : base(null, default)
        {
        }

        public Boolean IsReadOnly { get; set; }
    }
}