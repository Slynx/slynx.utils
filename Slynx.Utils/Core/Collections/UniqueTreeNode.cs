﻿// 
// UniqueTreeNode.cs is a part of Slynx.Utils project.
// 
// Created by Slynx on 10/05/2020 23:13.

using System;
using System.Collections;
using System.Collections.Generic;

namespace Slynx.Utils.Core.Collections
{
    /// <summary>
    ///     Default <see cref="IUniqueTreeNode{TKey,TValue}" /> implementation.
    /// </summary>
    /// <typeparam name="TKey"></typeparam>
    /// <typeparam name="TValue"></typeparam>
    public class UniqueTreeNode<TKey, TValue> : IUniqueTreeNode<TKey, TValue>
    {
        private readonly Dictionary<TKey, IUniqueTreeNode<TKey, TValue>> nodes = new Dictionary<TKey, IUniqueTreeNode<TKey, TValue>>();

        public UniqueTreeNode()
        {
        }

        protected UniqueTreeNode(IUniqueTreeNode<TKey, TValue> parent, TKey key)
        {
            Parent = parent;
            Key = key;
        }

        /// <inheritdoc />
        public virtual TKey Key { get; }

        /// <inheritdoc />
        public virtual TValue Value { get; set; }

        /// <inheritdoc />
        public virtual IUniqueTreeNode<TKey, TValue> Parent { get; }

        /// <inheritdoc />
        public virtual IUniqueTree<TKey, TValue> Root
        {
            get
            {
                IUniqueTreeNode<TKey, TValue> parent = this;
                while (parent.Parent != null)
                {
                    parent = parent.Parent;
                }

                return parent as IUniqueTree<TKey, TValue>;
            }
        }

        /// <inheritdoc />
        public virtual IUniqueTreeNode<TKey, TValue> this[TKey key]
        {
            get
            {
                if (nodes.ContainsKey(key))
                {
                    return nodes[key];
                }

                return Add(key, default);
            }
        }

        /// <inheritdoc />
        public virtual IUniqueTreeNode<TKey, TValue> Add(TKey key, TValue value)
        {
            var root = Root;
            if (root == null)
            {
                throw new InvalidOperationException("TreeNode is not attachted to Tree");
            }

            if (root.IsReadOnly)
            {
                throw new InvalidOperationException("Cannot edit read only tree");
            }

            var node = CreateNew(this, key);
            node.Value = value;

            nodes.Add(key, node);
            return node;
        }

        /// <inheritdoc />
        public virtual Boolean Remove(TKey key)
        {
            return nodes.Remove(key);
        }

        /// <inheritdoc />
        public virtual Boolean Contains(TKey key)
        {
            return nodes.ContainsKey(key);
        }

        public void Clear()
        {
            nodes.Clear();
        }

        public Int32 Count
        {
            get { return nodes.Count; }
        }

        /// <inheritdoc />
        public IEnumerator<IUniqueTreeNode<TKey, TValue>> GetEnumerator()
        {
            return nodes.Values.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        /// <summary>
        ///     Creates new element. Use this method rather than standard object creation to allow for easier item inheritance.
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        protected virtual IUniqueTreeNode<TKey, TValue> CreateNew(IUniqueTreeNode<TKey, TValue> parent, TKey key)
        {
            return new UniqueTreeNode<TKey, TValue>(parent, key);
        }

        /// <summary>
        ///     Returns a string that represents the current object.
        /// </summary>
        /// <returns>
        ///     A string that represents the current object.
        /// </returns>
        public override String ToString()
        {
            return $"Key:{Key} Value:{Value}";
        }
    }
}