﻿// 
// IUniqueTreeNode.cs is a part of Slynx.Utils project.
// 
// Created by Slynx on 10/05/2020 23:13.

using System;
using System.Collections.Generic;

namespace Slynx.Utils.Core.Collections
{
    /// <summary>
    ///     TreeNode interface
    /// </summary>
    /// <typeparam name="TKey"></typeparam>
    /// <typeparam name="TValue"></typeparam>
    public interface IUniqueTreeNode<TKey, TValue> : IEnumerable<IUniqueTreeNode<TKey, TValue>>
    {
        /// <summary>
        ///     Node key, in most cases <see cref="string" />
        /// </summary>
        TKey Key { get; }

        TValue Value { get; set; }

        IUniqueTreeNode<TKey, TValue> Parent { get; }

        IUniqueTree<TKey, TValue> Root { get; }

        IUniqueTreeNode<TKey, TValue> this[TKey key] { get; }

        Int32 Count { get; }

        IUniqueTreeNode<TKey, TValue> Add(TKey key, TValue value);

        Boolean Remove(TKey key);

        Boolean Contains(TKey key);

        void Clear();
    }
}