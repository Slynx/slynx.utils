﻿// 
// Component.cs is a part of Slynx.Utils project.
// 
// Created by Slynx on 29/08/2021 02:29.

using System;
using System.Diagnostics;

namespace Slynx.Utils.Core
{
    [DebuggerDisplay("For: {ForType} Implemented: {ImplementedByType} Overrides: {OverridesType}")]
    public class Component
    {
        private Component()
        {
        }

        internal Type ForType { get; set; }

        internal Type ImplementedByType { get; set; }

        internal Type OverridesType { get; set; }

        public static Component For<T>()
        {
            return new Component { ForType = typeof(T) };
        }

        public Component ImplementedBy<T>()
        {
            ImplementedByType = typeof(T);
            return this;
        }

        public Component Overrides<T>()
        {
            OverridesType = typeof(T);
            return this;
        }
    }
}