﻿// 
// MathUtils.cs is a part of Slynx.Utils project.
// 
// Created by Slynx on 10/05/2020 23:13.

using System;

namespace Slynx.Utils.Core
{
    /// <summary>
    ///     Additional static methods not available in .Net <see cref="System.Math" /> class.
    /// </summary>
    public static class MathUtils
    {
        /// <summary>
        ///     Restricts a value to be within a specified range.
        ///     <remarks>
        ///         Method returns:
        ///         <paramref name="min" /> if &lt; 0, <paramref name="max" /> if &gt; 0, otherwise <paramref name="val" />
        ///     </remarks>
        /// </summary>
        /// <typeparam name="T">Type implementing <see cref="IComparable{T}" /> interface</typeparam>
        /// <param name="val"><typeparamref name="T" /> value to check</param>
        /// <param name="min">Minimum <typeparamref name="T" /> value</param>
        /// <param name="max">Maximum <typeparamref name="T" /> value</param>
        /// <returns>
        ///     <paramref name="min" /> if &lt; 0, <paramref name="max" /> if &gt; 0, otherwise <paramref name="val" />
        /// </returns>
        public static T Clamp<T>(T val, T min, T max) where T : IComparable<T>
        {
            if (val.CompareTo(min) < 0)
            {
                return min;
            }

            if (val.CompareTo(max) > 0)
            {
                return max;
            }

            return val;
        }
    }
}