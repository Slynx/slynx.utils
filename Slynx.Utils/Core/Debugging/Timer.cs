﻿// 
// Timer.cs is a part of Slynx.Utils project.
// 
// Created by Slynx on 10/05/2020 23:13.

using System;
using System.Diagnostics;
using System.Text;

namespace Slynx.Utils.Core.Debugging
{
    /// <summary>
    ///     Static helper methods to measure time for debugging purposes.
    /// </summary>
    public static class Timer
    {
        /// <summary>
        ///     Measure <see cref="Action" /> processing time and returns result as formatted string.
        /// </summary>
        /// <param name="action"></param>
        /// <param name="iterations"></param>
        /// <returns></returns>
        public static String Measure(Action action, Int32 iterations = 1)
        {
            if (action == null)
            {
                return String.Empty;
            }

            var st = new Stopwatch();
            st.Start();
            for (var i = 0; i < iterations; i++)
            {
                action();
            }

            st.Stop();
            var sb = new StringBuilder();
            sb.AppendLine($"Test executed {iterations} times");
            sb.AppendLine($"Total time: {st.ElapsedMilliseconds}ms");
            sb.AppendLine($"Total ticks: {st.ElapsedTicks}");
            if (iterations > 1)
            {
                sb.AppendLine($"Average time: {(st.ElapsedMilliseconds / iterations)}ms");
                sb.AppendLine($"Average ticks: {(st.ElapsedTicks / iterations)}");
            }

            return sb.ToString();
        }

        /// <summary>
        ///     Returns current time in detailed format, e.g. '07:27:15.018'
        /// </summary>
        /// <returns></returns>
        public static String CurrentTime()
        {
            return $"{DateTime.Now:hh:mm:ss.fff}";
        }
    }
}