﻿// 
// CsvWriter.cs is a part of Slynx.Utils project.
// 
// Created by Slynx on 04/02/2021 12:31.

using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;

namespace Slynx.Utils.Core.Parsers.Csv
{
    public class CsvWriter : IDisposable
    {
        private readonly Dictionary<Type, List<(Func<Object, Object>, CsvColumnAttribute)>> cache = new Dictionary<Type, List<(Func<Object, Object>, CsvColumnAttribute)>>();
        private readonly StreamWriter writer;
        private readonly Boolean disposeWriter = true;
        private String format;
        private Char? quote = '"';
        private Boolean writeHeader = true;

        public CsvWriter(StreamWriter writer, Boolean dispose = false)
        {
            this.writer = writer;
            disposeWriter = dispose;
        }

        public CsvWriter(Stream stream, Encoding encoding = null)
        {
            writer = new StreamWriter(stream, encoding ?? new UTF8Encoding(false)) { AutoFlush = true };
        }

        public CsvWriter(String path, Encoding encoding = null)
        {
            writer = new StreamWriter(new FileStream(path, FileMode.Create), encoding ?? new UTF8Encoding(false)) { AutoFlush = true };
        }

        public Int32 FixedColumnCount { get; set; } = -1;

        public Char Delimiter { get; set; } = ',';

        public Boolean IgnoreHeader { get; set; }

        public CultureInfo CultureInfo { get; set; } = CultureInfo.InvariantCulture;

        /// <summary>
        ///     Gets/sets the character used for column quotes.
        /// </summary>
        public Char? Quote
        {
            get { return quote; }
            set
            {
                quote = value;
                if (value == null)
                {
                    format = "{0}";
                }
                else
                {
                    format = value + "{0}" + value;
                }
            }
        }

        public void Dispose()
        {
            if (disposeWriter)
            {
                writer?.Dispose();
            }
        }

        public void WriteRow(Object row)
        {
            var type = row.GetType();
            if (!cache.ContainsKey(type))
            {
                cache[type] = new List<(Func<Object, Object>, CsvColumnAttribute)>();
                foreach (PropertyInfo info in type.GetProperties())
                {
                    var attrib = info.GetCustomAttribute<CsvColumnAttribute>(true);
                    if (attrib != null)
                    {
                        var minMax = MathUtils.Clamp(attrib.Index, 0, cache[type].Count);
                        cache[type].Insert(minMax, (BuildGetAccessor(info.GetGetMethod()), attrib));
                    }
                }
            }

            var values = new String[cache[type].Count];
            if (writeHeader && !IgnoreHeader)
            {
                var any = false;
                for (var i = 0; i < cache[type].Count; i++)
                {
                    if (cache[type][i].Item2.Header != null)
                    {
                        values[i] = cache[type][i].Item2.Header;
                        any = true;
                    }

                    if (any && cache[type][i].Item2.Header == null)
                    {
                        throw new Exception("Partial headers are not supported. Set header names for all columns or null them to ignore header");
                    }
                }

                WriteRow(values);
                writeHeader = false;
            }

            for (var i = 0; i < cache[type].Count; i++)
            {
                var val = cache[type][i].Item1.Invoke(row);
                if (val is IFormattable form)
                {
                    values[i] = form.ToString(cache[type][i].Item2.Format, CultureInfo);
                }
                else if (val is IConvertible con)
                {
                    values[i] = con.ToString(CultureInfo);
                }
                else
                {
                    values[i] = val?.ToString() ?? String.Empty;
                }
            }

            WriteRow(values);
        }

        private static Func<Object, Object> BuildGetAccessor(MethodInfo method)
        {
            var obj = Expression.Parameter(typeof(Object), "o");
            Expression<Func<Object, Object>> expr = Expression.Lambda<Func<Object, Object>>(Expression.Convert(Expression.Call(Expression.Convert(obj, method.DeclaringType), method), typeof(Object)), obj);
            return expr.Compile();
        }

        public void WriteRow(params String[] values)
        {
            var cols = FixedColumnCount == -1 ? values.Length : FixedColumnCount;

            for (var i = 0; i < cols; i++)
            {
                if (i > 0)
                {
                    writer.Write(Delimiter);
                }

                writer.Write(format, i < values.Length ? values[i] : String.Empty);
            }

            writer.WriteLine();
        }
    }
}