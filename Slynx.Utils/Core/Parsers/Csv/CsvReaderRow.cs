﻿// 
// CsvReaderRow.cs is a part of Slynx.Utils project.
// 
// Created by Slynx on 04/02/2021 12:31.

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq.Expressions;
using System.Reflection;

namespace Slynx.Utils.Core.Parsers.Csv
{
    public class CsvReaderRow : List<String>
    {
        private class Property
        {
            private readonly Action<Object, Object> setFunc;

            public Property(Type type, CsvColumnAttribute attribute, Action<Object, Object> set)
            {
                Attribute = attribute;
                PropertyType = type;
                setFunc = set;
            }

            public CsvColumnAttribute Attribute { get; }

            public Type PropertyType { get; }

            public void Set(Object instance, Object value)
            {
                setFunc(instance, value);
            }
        }

        private static readonly Dictionary<Type, List<Property>> cache = new Dictionary<Type, List<Property>>();
        private readonly CsvReaderRow header;
        private readonly CsvReader.ICustomTypeResolver resolver;

        internal CsvReaderRow(CsvReader.ICustomTypeResolver resolver, CsvReaderRow header, params String[] values)
        {
            this.resolver = resolver;
            this.header = header;
            AddRange(values);
        }

        public Boolean IsHeader { get; set; }

        public Int32 LineNumber { get; set; }

        private static Action<Object, Object> BuildSetAccessor(MethodInfo method)
        {
            var obj = Expression.Parameter(typeof(Object), "o");
            var val = Expression.Parameter(typeof(Object), "value");
            Expression<Action<Object, Object>> expr =
                Expression.Lambda<Action<Object, Object>>(Expression.Call(Expression.Convert(obj, method.DeclaringType), method, Expression.Convert(val, method.GetParameters()[0].ParameterType)), obj, val);
            return expr.Compile();
        }

        public T Get<T>() where T : new()
        {
            var type = typeof(T);
            if (!cache.ContainsKey(type))
            {
                cache[type] = new List<Property>();
                foreach (PropertyInfo info in type.GetProperties())
                {
                    var attrib = info.GetCustomAttribute<CsvColumnAttribute>(true);
                    if (attrib != null)
                    {
                        cache[type].Add(new Property(info.PropertyType, attrib, BuildSetAccessor(info.GetSetMethod())));
                    }
                }
            }

            var result = new T();

            foreach (Property p in cache[type])
            {
                Int32 index = -1;
                if (header != null && !String.IsNullOrWhiteSpace(p.Attribute.Header))
                {
                    index = header.IndexOf(p.Attribute.Header);
                }

                if (index == -1)
                {
                    index = p.Attribute.Index;
                }

                if (index != -1)
                {
                    if (index >= Count)
                    {
                        throw new ArgumentOutOfRangeException("index", "Indeks kolumny musi być mniejszy niż liczba kolumn");
                    }

                    p.Set(result, Get(p.PropertyType, index));
                }
                else
                {
                    throw new Exception($"Column with name '{p.Attribute.Header}' or index '{p.Attribute.Index}' not found!");
                }
            }

            return result;
        }

        private Object Get(Type type, Int32 index, CultureInfo cultureInfo = null)
        {
            var value = this[index];
            if (resolver != null)
            {
                return resolver.Get(value, type);
            }

            if (type == typeof(Double))
            {
                return Double.Parse(value, NumberStyles.AllowDecimalPoint, cultureInfo ?? CultureInfo.InvariantCulture);
            }

            if (type == typeof(Decimal))
            {
                return Decimal.Parse(value, NumberStyles.AllowDecimalPoint, cultureInfo ?? CultureInfo.InvariantCulture);
            }

            if (type == typeof(Int32))
            {
                return Int32.Parse(value, cultureInfo ?? CultureInfo.InvariantCulture);
            }

            return Convert.ChangeType(this[index], type);
        }

        public T Get<T>(Int32 index, CultureInfo cultureInfo = null)
        {
            return (T)Get(typeof(T), index, cultureInfo);
        }
    }
}