// 
// EmptyLineBehavior.cs is a part of Slynx.Utils project.
// 
// Created by Slynx on 04/02/2021 12:31.

namespace Slynx.Utils.Core.Parsers.Csv
{
    public enum HeaderBehavior
    {
        NoHeader,
        Ignore,
        AsFirstRecord
    }
}