﻿// 
// CsvColumnAttribute.cs is a part of Slynx.Utils project.
// 
// Created by Slynx on 05/02/2021 10:16.

using System;

namespace Slynx.Utils.Core.Parsers.Csv
{
    [AttributeUsage(AttributeTargets.Property)]
    public sealed class CsvColumnAttribute : Attribute
    {
        public CsvColumnAttribute(String header, Int32 index)
        {
            Header = header;
            Index = index;
        }

        public String Header { get; }

        public Int32 Index { get; }

        public String Format { get; set; }
    }
}