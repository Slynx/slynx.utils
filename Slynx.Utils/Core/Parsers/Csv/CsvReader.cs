// 
// CsvReader.cs is a part of Slynx.Utils project.
// 
// Created by Slynx on 04/02/2021 12:31.

using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Slynx.Utils.Core.Parsers.Csv
{
    public class CsvReader : IDisposable, IEnumerable<CsvReaderRow>
    {
        private class Enumerator : IEnumerator<CsvReaderRow>
        {
            private readonly CsvReader reader;

            private CsvReaderRow header;
            private Int32 recordCounter;

            public Enumerator(CsvReader reader)
            {
                this.reader = reader;
            }

            public void Dispose()
            {
            }

            public Boolean MoveNext()
            {
                var line = reader.reader.ReadLine();
                if (line == null)
                {
                    return false;
                }

                Current = new CsvReaderRow(reader.CustomTypeResolver, header);

                reader.LineNumber++;
                Current.LineNumber = ++recordCounter;
                var index = 0;
                var quote = false;
                var quoted = false;
                var start = 0;
                while (index < line.Length)
                {
                    if (quote && line[index] == reader.Quote)
                    {
                        quote = false;
                    }
                    else if (!quote && line[index] == reader.Quote)
                    {
                        quote = true;
                        quoted = true;
                        start = index + 1;
                    }
                    else if (line[index] == reader.Delimiter && !quote)
                    {
                        if (start >= 0)
                        {
                            Add(line.Substring(start, index - start + (quoted ? -1 : 0)));
                            start = index + 1;
                        }
                        else
                        {
                            start = index + 1;
                        }

                        quoted = false;
                    }

                    index++;
                }

                if (start >= 0)
                {
                    Add(line.Substring(start, index - start + (quoted ? -1 : 0)));
                }

                if (reader.LineNumber == 1)
                {
                    switch (reader.Header)
                    {
                        case HeaderBehavior.Ignore:
                        {
                            recordCounter--;
                            Current.IsHeader = true;
                            Current.LineNumber = 0;
                            header = Current;
                            return MoveNext();
                        }
                        case HeaderBehavior.NoHeader:
                        {
                            break;
                        }
                        case HeaderBehavior.AsFirstRecord:
                        {
                            recordCounter--;
                            Current.IsHeader = true;
                            Current.LineNumber = 0;
                            header = Current;
                            break;
                        }
                        default:
                        {
                            throw new ArgumentOutOfRangeException();
                        }
                    }
                }

                return true;
            }

            void IEnumerator.Reset()
            {
            }

            public CsvReaderRow Current { get; private set; }

            Object IEnumerator.Current
            {
                get { return Current; }
            }

            private void Add(String input)
            {
                Current.Add(reader.TrimWhitespaces ? input.Trim() : input);
            }
        }

        public interface ICustomTypeResolver
        {
            Object Get(String input, Type outputType);
        }

        private readonly StreamReader reader;

        private Enumerator enumerator;

        public CsvReader(Stream stream, Encoding encoding = null)
        {
            reader = new StreamReader(stream, encoding ?? new UTF8Encoding(false));
        }

        public CsvReader(String path, Encoding encoding = null)
        {
            reader = new StreamReader(new FileStream(path, FileMode.Open), encoding ?? new UTF8Encoding(false));
        }

        public ICustomTypeResolver CustomTypeResolver { get; set; }

        public Char Delimiter { get; set; } = ',';

        /// <summary>
        ///     Gets/sets the character used for column quotes.
        /// </summary>
        public Char Quote { get; set; } = '"';

        public Boolean TrimWhitespaces { get; set; } = true;

        public HeaderBehavior Header { get; set; }

        public Int32 LineNumber { get; set; }

        public void Dispose()
        {
            reader.Dispose();
        }

        IEnumerator<CsvReaderRow> IEnumerable<CsvReaderRow>.GetEnumerator()
        {
            if (enumerator != null)
            {
                throw new Exception($"Multiple enumerations are not supported. Create new {nameof(CsvReader)} object");
            }

            return enumerator = new Enumerator(this);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return (this as IEnumerable<CsvReaderRow>).GetEnumerator();
        }
    }
}