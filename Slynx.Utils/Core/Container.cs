// 
// Container.cs is a part of Slynx.Utils project.
// 
// Created by Slynx on 29/08/2021 02:29.

using System;
using System.Collections.Generic;
using System.Linq;

namespace Slynx.Utils.Core
{
    public class Container
    {
        private readonly Dictionary<Type, Component> components = new Dictionary<Type, Component>();

        public static Container Default { get; } = new Container();

        public void Register(Component component)
        {
            if (components.ContainsKey(component.ForType))
            {
                throw new ArgumentException($"Type {component.ForType.FullName} is already registered");
            }

            if (components.Values.FirstOrDefault(f => f.OverridesType != null && component.OverridesType != null && f.OverridesType == component.OverridesType) != null)
            {
                throw new ArgumentException($"Type {component.OverridesType.FullName} is already overridden by {component.ForType.FullName}");
            }

            components.Add(component.ForType, component);
        }

        public T Resolve<T>(params Object[] ctorArguments)
        {
            return (T)Activator.CreateInstance(GetCurrentType(typeof(T)), ctorArguments);
        }

        public Type GetCurrentType(Type type)
        {
            var t = type;
            var overridden = components.Values.FirstOrDefault(f => f.OverridesType == t);
            if (overridden != null)
            {
                return overridden.ImplementedByType ?? overridden.ForType;
            }

            var forType = components.Values.FirstOrDefault(f => f.ForType == t);
            if (forType != null)
            {
                if (forType.ImplementedByType != null)
                {
                    return GetCurrentType(forType.ImplementedByType);
                }

                return forType.ForType;
            }

            var implementedType = components.Values.FirstOrDefault(f => f.ImplementedByType == t);
            if (implementedType != null)
            {
                return implementedType.ImplementedByType;
            }

            throw new Exception("Implementation for type " + type.FullName + " not found");
        }
    }
}