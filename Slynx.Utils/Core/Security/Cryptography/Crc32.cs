﻿// 
// Crc32.cs is a part of Slynx.Utils project.
// 
// Created by Slynx on 10/05/2020 23:13.

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Threading;
using System.Threading.Tasks;

namespace Slynx.Utils.Core.Security.Cryptography
{
    /// <summary>
    ///     CRC32 checksum algorithm implementation.
    /// </summary>
    public class Crc32 : HashAlgorithm
    {
        /// <summary>
        ///     Internal helper for multi-core calculation and partial checksum combination.
        /// </summary>
        private static class Helper
        {
            /// <summary>
            ///     Base polynomial used in lookup <see cref="table" /> generation.
            ///     It's a default, used in archivization, network, etc.
            /// </summary>
            public const UInt32 BasePolynomial = 0xEDB88320;

            /// <summary>
            ///     Initial polynomial for every calculation.
            /// </summary>
            public const UInt32 InitPolynomial = 0xFFFFFFFF;

            /// <summary>
            ///     Minimum number of bytes to create thread for data in multi-core calculation.
            ///     If data length is less than this value, data computation will be redirected to standard method.
            /// </summary>
            internal const Int32 ThreadCost = 262144;

            /// <summary>
            ///     Static Table with classic CRC32 lookup table and additional tables for optimized
            ///     calculation.
            /// </summary>
            private static readonly UInt32[] table;

            /// <summary>
            ///     Number of processors on the current machine.
            ///     Because we want to fully use our threads, their count shouldn't exceed number of physical cores.
            ///     Running two threads on this same core is meaningless.
            /// </summary>
            internal static readonly Int32 ThreadCount = Environment.ProcessorCount;

            // For combine
            private static UInt32[] evenCache;
            private static UInt32[] oddCache;

            static Helper()
            {
                unchecked
                {
                    table = new UInt32[8 * 256];
                    UInt32 i;
                    for (i = 0; i < 256; i++) // Build the classic CRC32 lookup table.
                    {
                        var c = i;
                        for (var j = 0; j < 8; j++)
                        {
                            c = (c & 1) != 0 ? (c >> 1) ^ BasePolynomial : (c >> 1);
                        }

                        table[i] = c;
                    }

                    for (; i < 256 * 8; i++) // Build additional lookup tables.
                    {
                        var c = table[i - 256];
                        table[i] = table[c & 0xFF] ^ (c >> 8);
                    }
                }

                PrepareEvenOddCache();
                MultiCoreFileBufferSize = 100 * 1024 * 1024; // 100 MB per Core
            }

            public static Int32 MultiCoreFileBufferSize { get; }

            /// <summary>
            ///     Returns number of bytes to calculate per thread.
            ///     This value is computed using <see cref="ThreadCount" /> and <see cref="ThreadCost" /> informations.
            /// </summary>
            /// <param name="count">Total data length to compute</param>
            /// <returns></returns>
            internal static Int64 GetBytesForThread(Int64 count)
            {
                var threadCount = ThreadCount;
                // Optimum value should be just count divided by ThreadCount
                var bytesPerThread = count / threadCount;

                // ... but we need to check if bytes count is not less than ThreadCost
                // 
                while (bytesPerThread < ThreadCost)
                {
                    bytesPerThread *= 2;
                }

                return bytesPerThread;
            }

            internal static UInt32 ComputeBlock(UInt32 crc, Byte[] data, Int32 offset, Int32 count)
            {
                Debug.Assert(data != null && offset >= 0 && count >= 0);

                for (; (offset & 7) != 0 && count != 0; count--)
                {
                    crc = (crc >> 8) ^ table[(Byte)crc ^ data[offset++]];
                }

                if (count >= 8)
                {
                    var to = (count - 8) & ~7;
                    count -= to;
                    to += offset;

                    while (offset != to)
                    {
                        crc ^= (UInt32)(data[offset] + (data[offset + 1] << 8) + (data[offset + 2] << 16) + (data[offset + 3] << 24));
                        var high = (UInt32)(data[offset + 4] + (data[offset + 5] << 8) + (data[offset + 6] << 16) + (data[offset + 7] << 24));
                        offset += 8;

                        crc = table[(Byte)crc + 0x700] ^ table[(Byte)(crc >>= 8) + 0x600] ^ table[(Byte)(crc >>= 8) + 0x500] ^ table[(Byte)(crc >> 8) + 0x400] ^ table[(Byte)(high) + 0x300] ^
                              table[(Byte)(high >>= 8) + 0x200] ^ table[(Byte)(high >>= 8) + 0x100] ^ table[(Byte)(high >> 8) + 0x000];
                    }
                }

                while (count-- != 0)
                {
                    crc = (crc >> 8) ^ table[(Byte)crc ^ data[offset++]];
                }

                return crc;
            }

            /// <summary>
            ///     Function to combine two crc's.
            ///     First crc is base crc, eg. from first data part or computed before.
            ///     Second crc is genereated crc to combine.
            ///     Length is length of data used to compute crc2.
            /// </summary>
            /// <param name="crc1"></param>
            /// <param name="crc2"></param>
            /// <param name="length"></param>
            /// <returns></returns>
            public static UInt32 Combine(UInt32 crc1, UInt32 crc2, Int32 length)
            {
                Debug.Assert(evenCache != null && oddCache != null);

                if (length <= 0)
                {
                    return crc1;
                }

                if (crc1 == InitPolynomial)
                {
                    return crc2;
                }

                var even = CopyArray(evenCache);
                var odd = CopyArray(oddCache);

                crc1 = ~crc1;
                crc2 = ~crc2;

                var len2 = (UInt32)length;

                do
                {
                    MatrixSquare(even, odd);

                    if ((len2 & 1) != 0)
                    {
                        crc1 = MatrixTimes(even, crc1);
                    }

                    len2 >>= 1;

                    if (len2 == 0)
                    {
                        break;
                    }

                    MatrixSquare(odd, even);
                    if ((len2 & 1) != 0)
                    {
                        crc1 = MatrixTimes(odd, crc1);
                    }

                    len2 >>= 1;
                } while (len2 != 0);

                crc1 ^= crc2;
                return ~crc1;
            }

            private static UInt32[] CopyArray(UInt32[] a)
            {
                var b = new UInt32[a.Length];
                Buffer.BlockCopy(a, 0, b, 0, a.Length * sizeof(UInt32));
                return b;
            }

            private static void PrepareEvenOddCache()
            {
                var even = new UInt32[32];
                var odd = new UInt32[32];

                odd[0] = BasePolynomial; // the CRC-32 polynomial
                for (var i = 1; i < 32; i++)
                {
                    odd[i] = 1U << (i - 1);
                }

                // put operator for two zero bits in even
                MatrixSquare(even, odd);

                // put operator for four zero bits in odd
                MatrixSquare(odd, even);

                oddCache = odd;
                evenCache = even;
            }

            /// <param name="square">this array will be modified!</param>
            /// <param name="mat">will not be modified</param>
            private static void MatrixSquare(UInt32[] square, UInt32[] mat)
            {
                for (var i = 0; i < 32; i++)
                {
                    square[i] = MatrixTimes(mat, mat[i]);
                }
            }

            /// <param name="matrix">will not be modified</param>
            /// <param name="vec"></param>
            private static UInt32 MatrixTimes(UInt32[] matrix, UInt32 vec)
            {
                UInt32 sum = 0;
                var j = 0;
                while (vec != 0)
                {
                    if ((vec & 1) != 0)
                    {
                        sum ^= matrix[j];
                    }

                    vec >>= 1;
                    j++;
                }

                return sum;
            }
        }

        private class BlockComputeTask
        {
            private readonly Crc32 crc;

            public BlockComputeTask(Crc32 crc)
            {
                this.crc = crc;
            }

            public UInt32 Result { get; private set; }

            public Task ComputeTask { get; private set; }

            public Int32 Read { get; set; }

            public void Compute(ArraySegment<Byte> data)
            {
                Read = data.Count;
                ComputeTask = Task.Factory.StartNew(() => crc.ComputeAndUpdate(data), TaskCreationOptions.PreferFairness).ContinueWith(ContinuationAction);
            }

            private void ContinuationAction(Task<UInt32> task)
            {
                Result = task.Result;
                GC.Collect();
            }
        }

        /// <summary>
        ///     Minimum buffer size for reading data chunks from stream.
        ///     The default value is 4KB.
        /// </summary>
        public Int32 MinimumReadBufferSize { get; set; } = 4 << 10;

        /// <summary>
        ///     Maximum buffer size for reading data chunks from stream.
        ///     The default value is 64MB.
        /// </summary>
        public Int32 MaximumReadBufferSize { get; set; } = 64 << 20;

        /// <summary>
        ///     Maximum allowed buffer size in bytes for on task in multicore computation.
        /// </summary>
        private const Int32 MaximumMultiCoreTaskBufferSize = 256 << 20;

        /// <summary>
        ///     Currently computed crc32. It's used only internally. Final value is in
        ///     <see cref="System.Security.Cryptography.HashAlgorithm.Hash" />.
        /// </summary>
        private UInt32 computed;

        /// <summary>
        ///     Total data length to compute.
        ///     Used to generate progress with multi-core methods.
        /// </summary>
        private Int64 progDataLength;

        /// <summary>
        ///     Currently total read data by all tasks.
        ///     Used to generate progress with multi-core methods.
        /// </summary>
        private Int64 progDataRead;


        /// <summary>
        ///     Last generated progress percentage value.
        ///     Used to generate progress with multi-core methods.
        /// </summary>
        private Int64 progLast;

        public override Int32 HashSize
        {
            get { return 32; }
        }

        /// <summary>
        ///     Use this method to combine partially generated control sum's to final value.
        /// </summary>
        /// <param name="crc1">First crc to combine</param>
        /// <param name="crc2">Second crc to combine</param>
        /// <param name="length">Data length used to generate second crc.</param>
        /// <returns>Combined <see cref="crc1" /> and <see cref="crc2" /></returns>
        public UInt32 Combine(UInt32 crc1, UInt32 crc2, Int32 length)
        {
            return Helper.Combine(crc1, crc2, length);
        }

        /// <summary>
        ///     Returns buffer length in bytes for reading data from stream.
        ///     Buffer size is optimized to have value equivalent to 100 calls for progress percentage event,
        ///     with some limitations and adjustments.
        /// </summary>
        /// <param name="length"></param>
        /// <returns></returns>
        private Int32 GetBufferSize(Int64 length)
        {
            // Buffer size is equal to length / 100, what is equivalent to 1%.
            // Stream length is unknown (0, Int64.MaxValue), so we need to stay with value in some range.
            return MathUtils.Clamp((Int32)length / 100, MinimumReadBufferSize, MaximumReadBufferSize);
        }

        /// <summary>
        ///     Default event invocator
        /// </summary>
        /// <param name="e"></param>
        protected virtual void OnProgressChanged(PercentageProgressChangedEventArgs e)
        {
            var handler = ProgressChanged;
            handler?.Invoke(this, e);
        }

        /// <summary>
        ///     Initialize progress accumulate method.
        /// </summary>
        /// <param name="totalDataLength"></param>
        private void InitializeProgressAccumulate(Int64 totalDataLength)
        {
            progDataLength = totalDataLength;
            progLast = -1;
            progDataRead = 0;
        }

        /// <summary>
        ///     Accumulate progress from multiple thread.
        ///     Used internally in <see cref="ComputeAndUpdate" />.
        /// </summary>
        /// <param name="read"></param>
        private void AccumulateProgress(Int32 read)
        {
            progDataRead += read;
            var currentProg = (Int32)(((Double)progDataRead / progDataLength) * 100);
            if (currentProg != progLast)
            {
                OnProgressChanged(new PercentageProgressChangedEventArgs(currentProg));
                progLast = currentProg;
            }
        }

        /// <summary>
        ///     Core method to calculate part of data - represented as <see cref="ArraySegment{Byte}" /> - on thread with
        ///     <see cref="Task" />.
        /// </summary>
        /// <param name="data"></param>
        /// <returns>
        ///     Calculated CRC32 from base <see cref="Helper.InitPolynomial" />. This value can be combined with other to
        ///     create CRC32 for whole data from parts.
        /// </returns>
        private UInt32 ComputeAndUpdate(ArraySegment<Byte> data)
        {
            var count = data.Count;
            var offset = data.Offset;

            // We create buffer size from total data length, not just out part to compute.
            // Creating sub-length buffer for every slice of data sent to all cores (logical processors) 
            // can significantly affect performance (event).
            var bufferLength = GetBufferSize(data.Array.Length);
            var crc = Helper.InitPolynomial;

            while (count > 0)
            {
                bufferLength = Math.Min(bufferLength, count);
                crc = Helper.ComputeBlock(crc, data.Array, offset, bufferLength);
                offset += bufferLength;
                count -= bufferLength;
                AccumulateProgress(bufferLength);
            }

            return crc;
        }

        /// <summary>
        ///     Initializes an implementation of the <see cref="T:System.Security.Cryptography.HashAlgorithm" /> class.
        /// </summary>
        public override void Initialize()
        {
            computed = Helper.InitPolynomial;
        }

        /// <summary>
        ///     When overridden in a derived class, routes data written to the object into the hash algorithm for computing the
        ///     hash.
        /// </summary>
        /// <param name="data">The input to compute the hash code for. </param>
        /// <param name="offset">The offset into the byte array from which to begin using data. </param>
        /// <param name="count">The number of bytes in the byte array to use as data. </param>
        protected override void HashCore(Byte[] data, Int32 offset, Int32 count)
        {
            computed = Helper.ComputeBlock(Helper.InitPolynomial, data, offset, count);
        }

        /// <summary>
        ///     When overridden in a derived class, finalizes the hash computation after the last data is processed by the
        ///     cryptographic stream object.
        /// </summary>
        /// <returns>
        ///     The computed hash code.
        /// </returns>
        protected override Byte[] HashFinal()
        {
            computed = ~computed;
            return BitConverter.GetBytes(computed);
        }

        /// <summary>
        ///     Progress changed event based on <see cref="PercentageProgressChangedEventArgs" /> class with value in range 0-100.
        ///     Event is used only in <see cref="HashAlgorithm" /> implementation method. Progress from methods from base class
        ///     <see cref="HashAlgorithm" />
        ///     cannot be tracked.
        /// </summary>
        public event EventHandler<PercentageProgressChangedEventArgs> ProgressChanged;

        /// <summary>
        ///     Compute the hash value for the specified region of the specified <c>byte</c> array.
        /// </summary>
        /// <param name="data">The input to compute hash code for.</param>
        /// <param name="offset">The offset into the byte array from which to begin using data.</param>
        /// <param name="count">The number of bytes in the array to use as data.</param>
        public void Compute(Byte[] data, Int32 offset, Int32 count)
        {
            Initialize();
            var bufferLength = GetBufferSize(data.Length);
            var length = data.Length;

            while (count > 0)
            {
                bufferLength = Math.Min(bufferLength, count);
                computed = Helper.ComputeBlock(computed, data, offset, bufferLength);
                OnProgressChanged(new PercentageProgressChangedEventArgs(offset, length));
                offset += bufferLength;
                count -= bufferLength;
            }

            HashValue = HashFinal();
        }

        /// <summary>
        ///     Compute the hash value for the specified <c>byte</c> array.
        /// </summary>
        /// <param name="data">The input to compute hash code for.</param>
        public void Compute(Byte[] data)
        {
            Compute(data, 0, data.Length);
        }

        /// <summary>
        ///     Compute the hash value for the specified <see cref="System.IO.Stream" /> object.
        /// </summary>
        /// <param name="inputStream">The input to compute hash code for.</param>
        public void Compute(Stream inputStream)
        {
            Initialize();
            var length = inputStream.Length;
            var bufferLength = GetBufferSize(length);
            Int64 total = 0;
            Int32 read;
            var buffer = new Byte[bufferLength];

            while ((read = inputStream.Read(buffer, 0, bufferLength)) > 0)
            {
                computed = Helper.ComputeBlock(computed, buffer, 0, read);
                total += read;
                OnProgressChanged(new PercentageProgressChangedEventArgs(total, length));
            }

            HashValue = HashFinal();
        }

        private static Byte[] CreateBuffer(Int32 length)
        {
            Byte[] buffer = null;

            while (buffer == null)
            {
                try
                {
                    buffer = new Byte[length];
                }
                catch (OutOfMemoryException)
                {
                    Thread.Sleep(100);
                    GC.Collect();
                }
            }

            return buffer;
        }

        /// <summary>
        ///     Compute the hash value for the specified <see cref="System.IO.Stream" /> object.
        ///     This method is used only if computation thread cost is less than data computation time,
        ///     what means that for small amounts of data <see cref="HashAlgorithm.Compute(System.IO.Stream)" /> is called
        ///     internally to maximize performance.
        /// </summary>
        /// <param name="inputStream">The input to compute hash code for.</param>
        public void ComputeMultiCore(Stream inputStream)
        {
            var count = inputStream.Length;
            // If data length is less than thread creation cost or machine have only one physical core
            // there is no sense in using multi-threaded method. We just redirect to standard computation method.
            if (count <= Helper.ThreadCost || Helper.ThreadCount <= 1)
            {
                Compute(inputStream);
                return;
            }

            Initialize();
            InitializeProgressAccumulate(count);
            var bufferLength = (Int32)Math.Min(MaximumMultiCoreTaskBufferSize, Helper.GetBytesForThread(count));
            Int32 read;
            var buffer = new Byte[bufferLength];

            var set = new HashSet<BlockComputeTask>();
            while ((read = inputStream.Read(buffer, 0, bufferLength)) > 0)
            {
                var computeTask = new BlockComputeTask(this);
                computeTask.Compute(new ArraySegment<Byte>(buffer, 0, read));
                set.Add(computeTask);

                buffer = CreateBuffer(bufferLength);
            }

            // We wait to complete computing on all our data slices...
            Task.WaitAll(set.Select(s => s.ComputeTask).ToArray());

            // Combine all parts to total value.
            foreach (var task in set)
            {
                computed = Helper.Combine(computed, task.Result, task.Read);
            }

            // ... and HashFinal call...
            HashValue = HashFinal();
        }

        /// <summary>
        ///     Returns generated hash code formatted as <see cref="T" /> type.
        ///     <example>
        ///         <code>
        /// Format &lt;<see cref="String" />&gt;() // returns "FFFFFFFF"
        /// Format &lt;<see cref="UInt32" />&gt;() // returns 4294967295
        /// </code>
        ///     </example>
        /// </summary>
        /// <typeparam name="T">The output type the hash code should be formatted as, e.g. <c>string</c> or <c>int</c></typeparam>
        /// <exception cref="System.ArgumentOutOfRangeException">Thrown when type is not supported</exception>
        /// <returns></returns>
        public T Format<T>()
        {
            if (typeof(T) == typeof(String))
            {
                return (T)(Object)BitConverter.ToUInt32(HashValue, 0).ToString("X");
            }

            if (typeof(T) == typeof(UInt32))
            {
                return (T)(Object)BitConverter.ToUInt32(HashValue, 0);
            }

            if (typeof(T) == typeof(Byte[]))
            {
                return (T)(Object)HashValue;
            }

            throw new ArgumentOutOfRangeException(nameof(T), "Type '" + typeof(T) + "' is not supported");
        }
    }
}