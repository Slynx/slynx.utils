﻿// 
// MD5Hash.cs is a part of Slynx.Utils project.
// 
// Created by Slynx on 17/08/2021 00:37.

using System;
using System.Security.Cryptography;
using System.Text;

namespace Slynx.Utils.Core.Security.Cryptography
{
    /// <summary>
    ///     Provides simple method to hash <see cref="string" /> with MD5 algorithm.
    /// </summary>
    public static class MD5Hash
    {
        /// <summary>
        ///     Create hash <see cref="String" /> from input.
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static String Create(String input)
        {
            if (String.IsNullOrEmpty(input))
            {
                return String.Empty;
            }

            using (var hasher = MD5.Create())
            {
                var data = hasher.ComputeHash(Encoding.UTF8.GetBytes(input));

                var builder = new StringBuilder();

                for (var i = 0; i < data.Length; i++)
                {
                    builder.Append(data[i].ToString("x2"));
                }

                return builder.ToString();
            }
        }

        /// <summary>
        ///     Generates hash from <see cref="input" /> and compares to <see cref="hash" />.
        /// </summary>
        /// <param name="input"></param>
        /// <param name="hash"></param>
        /// <returns></returns>
        public static Boolean Compare(String input, String hash)
        {
            return String.Equals(Create(input), hash, StringComparison.OrdinalIgnoreCase);
        }
    }
}