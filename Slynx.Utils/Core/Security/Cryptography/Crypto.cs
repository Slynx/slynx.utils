﻿// 
// Crypto.cs is a part of Slynx.Utils project.
// 
// Created by Slynx on 23/03/2021 02:16.

using System;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace Slynx.Utils.Core.Security.Cryptography
{
    /// <summary>
    ///     Simple crypto class based on AES algorithm for string encryption/decryption.
    /// </summary>
    public static class Crypto
    {
        private static readonly Byte[] salt = Encoding.ASCII.GetBytes("o6806642kbM7c5");

        /// <summary>
        ///     Encrypt the given string using AES. The string can be decrypted using
        ///     Decrypt(). The sharedSecret parameters must match.
        /// </summary>
        /// <param name="plainText">The text to encrypt.</param>
        /// <param name="sharedSecret">A password used to generate a key for encryption.</param>
        public static String Encrypt(String plainText, String sharedSecret)
        {
            if (String.IsNullOrEmpty(plainText))
            {
                throw new ArgumentNullException(nameof(plainText));
            }

            if (String.IsNullOrEmpty(sharedSecret))
            {
                throw new ArgumentNullException(nameof(sharedSecret));
            }

            // generate the key from the shared secret and the salt
            using (var key = new Rfc2898DeriveBytes(sharedSecret, salt))
            {
                // Create a RijndaelManaged object
                using (var aesAlg = TripleDES.Create())
                {
                    aesAlg.BlockSize = 64;
                    aesAlg.Key = key.GetBytes(aesAlg.KeySize / 8);
                    aesAlg.Padding = PaddingMode.PKCS7;

                    // Create a decryptor to perform the stream transform.
                    var encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV);

                    // Create the streams used for encryption.
                    using (var msEncrypt = new MemoryStream())
                    {
                        // prepend the IV
                        msEncrypt.Write(BitConverter.GetBytes(aesAlg.IV.Length), 0, sizeof(Int32));
                        msEncrypt.Write(aesAlg.IV, 0, aesAlg.IV.Length);
                        using (var csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                        {
                            using (var swEncrypt = new StreamWriter(csEncrypt))
                            {
                                //Write all data to the stream.
                                swEncrypt.Write(plainText);
                            }
                        }

                        return Convert.ToBase64String(msEncrypt.ToArray());
                    }
                }
            }
        }

        /// <summary>
        ///     Decrypt the given string.  Assumes the string was encrypted using
        ///     Encrypt(), using an identical sharedSecret.
        /// </summary>
        /// <param name="cipherText">The text to decrypt.</param>
        /// <param name="sharedSecret">A password used to generate a key for decryption.</param>
        public static String Decrypt(String cipherText, String sharedSecret)
        {
            if (String.IsNullOrEmpty(cipherText))
            {
                throw new ArgumentNullException(nameof(cipherText));
            }

            if (String.IsNullOrEmpty(sharedSecret))
            {
                throw new ArgumentNullException(nameof(sharedSecret));
            }

            // generate the key from the shared secret and the salt
            using (var key = new Rfc2898DeriveBytes(sharedSecret, salt))
            {
                // Create the streams used for decryption.
                var bytes = Convert.FromBase64String(cipherText);
                using (var msDecrypt = new MemoryStream(bytes))
                {
                    // Create a RijndaelManaged object
                    // with the specified key and IV.
                    using (var aesAlg = TripleDES.Create())
                    {
                        aesAlg.BlockSize = 64;
                        aesAlg.Key = key.GetBytes(aesAlg.KeySize / 8);
                        aesAlg.Padding = PaddingMode.PKCS7;

                        // Get the initialization vector from the encrypted stream
                        aesAlg.IV = ReadByteArray(msDecrypt);
                        // Create a decryptor to perform the stream transform.
                        var decryptor = aesAlg.CreateDecryptor(aesAlg.Key, aesAlg.IV);
                        using (var csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                        {
                            using (var srDecrypt = new StreamReader(csDecrypt))
                            {
                                // Read the decrypted bytes from the decrypting stream
                                // and place them in a string.
                                return srDecrypt.ReadToEnd();
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        ///     Fast base64 string pre-check.
        ///     Prechecking is not 100% accurate, it just rejects 99% of incorrect strings in non base64 format, where it's
        ///     obvious.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static Boolean IsBase64Fast(String value)
        {
            // Based on Oybek answer from http://stackoverflow.com/a/23955827

            if (String.IsNullOrEmpty(value) || value.Length % 4 != 0 || value.Contains(' ') || value.Contains('\t') || value.Contains('\r') || value.Contains('\n'))
            {
                return false;
            }

            var index = value.Length - 1;
            if (value[index] == '=')
            {
                index--;
            }

            if (value[index] == '=')
            {
                index--;
            }

            for (var i = 0; i <= index; i++)
            {
                if (IsInvalid(value[i]))
                {
                    return false;
                }
            }

            return true;
        }

        private static Boolean IsInvalid(Char value)
        {
            var intValue = (Int32)value;
            if (intValue >= 48 && intValue <= 57)
            {
                return false;
            }

            if (intValue >= 65 && intValue <= 90)
            {
                return false;
            }

            if (intValue >= 97 && intValue <= 122)
            {
                return false;
            }

            return intValue != 43 && intValue != 47;
        }

        private static Byte[] ReadByteArray(Stream s)
        {
            var rawLength = new Byte[sizeof(Int32)];
            if (s.Read(rawLength, 0, rawLength.Length) != rawLength.Length)
            {
                throw new Exception("Stream did not contain properly formatted byte array");
            }

            var buffer = new Byte[BitConverter.ToInt32(rawLength, 0)];
            if (s.Read(buffer, 0, buffer.Length) != buffer.Length)
            {
                throw new Exception("Did not read byte array properly");
            }

            return buffer;
        }
    }
}