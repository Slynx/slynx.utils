﻿// 
// Exposed.cs is a part of Slynx.Utils project.
// 
// Created by Slynx on 31/12/2020 09:38.

using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Reflection;
using Slynx.Utils.ExtensionMethods;

namespace Slynx.Utils
{
    public class Exposed : DynamicObject
    {
        private abstract class MemberInfoExtended
        {
            public abstract MemberInfo MemberInfo { get; }

            public abstract Object GetValue(Object instance, Object[] index = null);

            public abstract void SetValue(Object instance, Object value, Object[] index = null);
        }

        private class PropertyInfoExtended : MemberInfoExtended
        {
            private readonly PropertyInfo info;

            public PropertyInfoExtended(PropertyInfo info)
            {
                this.info = info;
            }

            public override MemberInfo MemberInfo
            {
                get { return info; }
            }

            public override Object GetValue(Object instance, Object[] index = null)
            {
                return info.GetValue(instance, index);
            }

            public override void SetValue(Object instance, Object value, Object[] index = null)
            {
                info.SetValue(instance, value, index);
            }
        }

        private class FieldInfoExtended : MemberInfoExtended
        {
            private readonly FieldInfo info;

            public FieldInfoExtended(FieldInfo info)
            {
                this.info = info;
            }

            public override MemberInfo MemberInfo
            {
                get { return info; }
            }

            public override Object GetValue(Object instance, Object[] index = null)
            {
                return info.GetValue(instance);
            }

            public override void SetValue(Object instance, Object value, Object[] index = null)
            {
                info.SetValue(instance, value);
            }
        }

        private const BindingFlags Flags = BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic;
        private static readonly IDictionary<Type, IDictionary<String, List<MemberInfoExtended>>> dictionary = new Dictionary<Type, IDictionary<String, List<MemberInfoExtended>>>();
        private static readonly IDictionary<Type, IDictionary<String, List<MethodInfo>>> methods = new Dictionary<Type, IDictionary<String, List<MethodInfo>>>();
        private readonly Type sourceType;

        public Exposed(Object source)
        {
            Source = source ?? throw new ArgumentNullException(nameof(source));
            if (Source is Type t)
            {
                sourceType = t;
            }
            else
            {
                sourceType = source.GetType();
            }
        }

        private Object Source { get; }

        private Object InternalGetSource(MemberInfo info)
        {
            if (Source is Type)
            {
                if (info is PropertyInfo pi)
                {
                    if ((pi.GetSetMethod(true)?.IsStatic ?? false) || (pi.GetSetMethod(true)?.IsStatic ?? false))
                    {
                        return null;
                    }
                }

                if (info is MethodInfo mi)
                {
                    return mi.IsStatic ? null : Source;
                }
            }

            return Source;
        }

        public override Boolean TryGetMember(GetMemberBinder binder, out Object result)
        {
            MemberInfoExtended member = GetPropertyOrField(binder.Name, null);
            result = member.GetValue(InternalGetSource(member.MemberInfo))?.AsExposed();
            return true;
        }

        public override Boolean TrySetMember(SetMemberBinder binder, Object value)
        {
            var member = GetPropertyOrField(binder.Name, null);
            if (value is Exposed ex)
            {
                value = ex.Source;
            }

            member.SetValue(InternalGetSource(member.MemberInfo), value);
            return true;
        }

        public override Boolean TryGetIndex(GetIndexBinder binder, Object[] indexes, out Object result)
        {
            var member = GetPropertyOrField("Item", indexes);
            result = member.GetValue(InternalGetSource(member.MemberInfo), indexes)?.AsExposed();
            return true;
        }

        public override Boolean TrySetIndex(SetIndexBinder binder, Object[] indexes, Object value)
        {
            var member = GetPropertyOrField("Item", indexes);
            if (value is Exposed ex)
            {
                value = ex.Source;
            }

            member.SetValue(InternalGetSource(member.MemberInfo), value, indexes);
            return true;
        }

        public override Boolean TryInvokeMember(InvokeMemberBinder binder, Object[] args, out Object result)
        {
            for (var i = 0; i < args.Length; i++)
            {
                if (args[i] is Exposed e)
                {
                    args[i] = e.Source;
                }
            }

            var method = GetBestMethod(binder.Name, args);
            result = method.Invoke(InternalGetSource(method), args)?.AsExposed();
            return true;
        }


        private void CollectMethods(Type type, IDictionary<String, List<MethodInfo>> items)
        {
            foreach (MethodInfo method in type.GetMethods(Flags))
            {
                if (!items.ContainsKey(method.Name))
                {
                    items[method.Name] = new List<MethodInfo>();
                }

                items[method.Name].Add(method);
            }

            if (type.BaseType != null)
            {
                CollectMethods(type.BaseType, items);
            }
        }

        public override Boolean TryConvert(ConvertBinder binder, out Object result)
        {
            if (binder.Type.IsInstanceOfType(Source))
            {
                result = Source;
                return true;
            }

            result = Convert.ChangeType(Source, binder.Type);
            return true;
        }

        public override String ToString()
        {
            return Source?.ToString() ?? "null";
        }

        public Object ToObject()
        {
            return Source;
        }

        public T Cast<T>()
        {
            return (T)Source;
        }

        private MemberInfoExtended GetPropertyOrField(String name, Object[] indexes)
        {
            if (!dictionary.ContainsKey(sourceType))
            {
                dictionary[sourceType] = new Dictionary<String, List<MemberInfoExtended>>();
                CollectPropertiesAndFields(sourceType, dictionary[sourceType]);
            }

            if (dictionary[sourceType].TryGetValue(name, out List<MemberInfoExtended> result))
            {
                if (result.Count == 1)
                {
                    return result[0];
                }
                return GetBestMemberInternal(result, indexes ?? Array.Empty<Object>());
            }

            throw new MissingMemberException(sourceType.Name, name);
        }

        private static ParameterInfo[] GetParameters(MemberInfoExtended info)
        {
            if (info.MemberInfo is PropertyInfo pi)
            {
                return pi.GetIndexParameters();
            }

            if (info.MemberInfo is MethodInfo mi)
            {
                return mi.GetParameters();
            }

            return Array.Empty<ParameterInfo>();
        }

        private static MemberInfoExtended GetBestMemberInternal(IEnumerable<MemberInfoExtended> result, Object[] args)
        {
            foreach (MemberInfoExtended info in result)
            {
                var pars = GetParameters(info);
                if (pars.Length != args.Length)
                {
                    continue;
                }

                var ok = true;
                for (var i = 0; i < pars.Length; i++)
                {
                    if (args[i] is null)
                    {
                        continue;
                    }

                    var parType = pars[i].ParameterType;
                    if (pars[i].ParameterType.IsByRef)
                    {
                        parType = pars[i].ParameterType.GetElementType();
                    }

                    if (!parType?.IsInstanceOfType(args[i]) ?? true)
                    {
                        ok = false;
                        break;
                    }
                }

                if (ok)
                {
                    return info;
                }
            }

            return null;
        }

        private MethodInfo GetBestMethod(String name, Object[] args)
        {
            if (!methods.ContainsKey(sourceType))
            {
                methods[sourceType] = new Dictionary<String, List<MethodInfo>>();
                CollectMethods(sourceType, methods[sourceType]);
            }

            if (methods[sourceType].TryGetValue(name, out List<MethodInfo> result))
            {
                foreach (MethodInfo info in result)
                {
                    var pars = info.GetParameters();
                    if (pars.Length != args.Length)
                    {
                        continue;
                    }

                    for (var i = 0; i < pars.Length; i++)
                    {
                        if (args[i] is null)
                        {
                            continue;
                        }

                        var parType = args[i]?.GetType();
                        if (pars[i].ParameterType.IsByRef)
                        {
                            parType = pars[i].ParameterType.GetElementType();
                        }

                        if (!parType?.IsInstanceOfType(args[i]) ?? true)
                        {
                            var exp = pars.Aggregate(String.Empty, (s, parameterInfo) => s + ", " + parameterInfo).Trim(',', ' ');
                            var got = args.Aggregate(String.Empty, (s, parameterInfo) => s + ", " + parameterInfo.GetType().Name).Trim(',', ' ');
                            throw new MissingMethodException($"Incorrect parameters for method {name} Expected({exp}) Passed({got})");
                        }
                    }

                    return info;
                }
            }

            throw new MissingMethodException(sourceType.Name, name);
        }

        private static void CollectPropertiesAndFields(Type type, IDictionary<String, List<MemberInfoExtended>> items)
        {
            foreach (FieldInfo field in type.GetFields(Flags))
            {
                if (!items.ContainsKey(field.Name))
                {
                    items[field.Name] = new List<MemberInfoExtended>();
                }
                if (!field.Name.StartsWith("<"))
                {
                    items[field.Name].Add(new FieldInfoExtended(field));
                }
            }

            foreach (PropertyInfo property in type.GetProperties(Flags))
            {
                if (!items.ContainsKey(property.Name))
                {
                    items[property.Name] = new List<MemberInfoExtended>();
                }
                items[property.Name].Add(new PropertyInfoExtended(property));
            }

            if (type.BaseType != null)
            {
                CollectPropertiesAndFields(type.BaseType, items);
            }
        }
    }
}