﻿// 
// AppDomainExtensions.cs is a part of Slynx.Utils project.
// 
// Created by Slynx on 10/05/2020 23:13.

using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;

namespace Slynx.Utils.ExtensionMethods
{
    public static class AppDomainExtensions
    {
        public static void AddAssemblyResolvePath(this AppDomain appDomain, Func<String> selector)
        {
            var selectors = (List<Func<String>>)appDomain.GetData("Selectors") ?? new List<Func<String>>();
            selectors.Add(selector);
            appDomain.SetData("Selectors", selectors);

            appDomain.AssemblyResolve -= CurrentDomainOnAssemblyResolve;
            appDomain.AssemblyResolve += CurrentDomainOnAssemblyResolve;
        }

        private static Assembly CurrentDomainOnAssemblyResolve(Object sender, ResolveEventArgs args)
        {
            if (sender is AppDomain appDomain)
            {
                var file = args.Name.Contains(",") ? $"{args.Name.Substring(0, args.Name.IndexOf(','))}.dll" : $"{args.Name}.dll";
                var selectors = (List<Func<String>>)appDomain.GetData("Selectors") ?? new List<Func<String>>();

                foreach (var path in selectors)
                {
                    var fullPath = Path.Combine(path(), file);
                    if (File.Exists(fullPath))
                    {
                        return Assembly.LoadFile(fullPath);
                    }
                }
            }

            return null;
        }
    }
}