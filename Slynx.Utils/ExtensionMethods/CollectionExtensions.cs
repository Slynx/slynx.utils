﻿// 
// CollectionExtensions.cs is a part of Slynx.Utils project.
// 
// Created by Slynx on 23/10/2020 01:47.

using System;
using System.Collections.Generic;
using System.Linq;

namespace Slynx.Utils.ExtensionMethods
{
    /// <summary>
    ///     Extension methods for <see cref="ICollection{T}" />.
    /// </summary>
    public static class CollectionExtensions
    {
        /// <summary>
        ///     Returns random element from <see cref="ICollection{T}" />.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="collection"></param>
        /// <param name="removeDrawn"> True if drawn element should be removed from collection </param>
        /// <exception cref="System.ArgumentNullException">Thrown when collection is null</exception>
        /// <exception cref="System.ArgumentOutOfRangeException">Thrown when collection is empty</exception>
        /// <returns></returns>
        public static T Draw<T>(this ICollection<T> collection, Boolean removeDrawn)
        {
            var element = collection.Draw();
            if (removeDrawn)
            {
                collection.Remove(element);
            }

            return element;
        }

        public static void AddRange<T>(this ICollection<T> collection, IEnumerable<T> items)
        {
            foreach (T item in items ?? Enumerable.Empty<T>())
            {
                collection.Add(item);
            }
        }
    }
}