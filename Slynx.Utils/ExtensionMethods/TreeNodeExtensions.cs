﻿// 
// TreeNodeExtensions.cs is a part of Slynx.Utils project.
// 
// Created by Slynx on 10/05/2020 23:13.

using System.Collections.Generic;
using System.Linq;
using Slynx.Utils.Core.Collections;

namespace Slynx.Utils.ExtensionMethods
{
    /// <summary>
    ///     Extension methods for <see cref="IUniqueTreeNode{TKey,TValue}" />
    /// </summary>
    public static class TreeNodeExtensions
    {
        /// <summary>
        ///     Returns all values from specific node and below.
        /// </summary>
        /// <typeparam name="TKey"></typeparam>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="node"></param>
        /// <returns></returns>
        public static IEnumerable<TValue> GetValues<TKey, TValue>(this IUniqueTreeNode<TKey, TValue> node)
        {
            if (node == null)
            {
                yield break;
            }

            yield return node.Value;

            foreach (TValue n in node.SelectMany(GetValues))
            {
                yield return n;
            }
        }


        /// <summary>
        ///     Returns all nodes from specific node and below.
        /// </summary>
        /// <typeparam name="TKey"></typeparam>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="node"></param>
        /// <returns></returns>
        public static IEnumerable<IUniqueTreeNode<TKey, TValue>> GetNodes<TKey, TValue>(this IUniqueTreeNode<TKey, TValue> node)
        {
            if (node == null)
            {
                yield break;
            }

            foreach (IUniqueTreeNode<TKey, TValue> treeNode in node)
            {
                yield return treeNode;
            }

            foreach (IUniqueTreeNode<TKey, TValue> x in node.SelectMany(GetNodes))
            {
                yield return x;
            }
        }
    }
}