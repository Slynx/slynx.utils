﻿// 
// FileInfoExtensions.cs is a part of Slynx.Utils project.
// 
// Created by Slynx on 19/02/2021 14:09.

using System;
using System.IO;
using System.Runtime.InteropServices;

namespace Slynx.Utils.ExtensionMethods
{
    /// <summary>
    ///     Extension methods for <see cref="FileInfo" />.
    /// </summary>
    public static class FileInfoExtensions
    {
        [DllImport("kernel32", CharSet = CharSet.Unicode, SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        internal static extern Boolean DeleteFile(String name);

        public static Boolean Unblock(this FileInfo info)
        {
            return DeleteFile(info.FullName + ":Zone.Identifier");
        }
    }
}