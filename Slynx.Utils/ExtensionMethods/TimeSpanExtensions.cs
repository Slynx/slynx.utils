﻿// 
// TimeSpanExtensions.cs is a part of Slynx.Utils project.
// 
// Created by Slynx on 23/12/2020 00:55.

using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Text;

namespace Slynx.Utils.ExtensionMethods
{
    /// <summary>
    ///     <see cref="TimeSpan" /> extensions.
    /// </summary>
    public static class TimeSpanExtensions
    {
        [SuppressMessage("ReSharper", "InconsistentNaming")]
        internal class Localizer_plPL : ITimeSpanLocalizer
        {
            public String FormatHour(Int32 hours)
            {
                var h = hours % 10;
                return Format(hours, hours == 1 ? "godzina" : h >= 2 && h <= 4 ? "godziny" : "godzin");
            }

            public String FormatYear(Int32 years)
            {
                var y = years % 10;
                return Format(years, y == 1 ? "rok" : y >= 2 && y <= 4 ? "lata" : "lat");
            }

            public String FormatMonth(Int32 months)
            {
                return Format(months, months == 1 ? "miesiąc" : months >= 2 && months <= 4 ? "miesiące" : "miesięcy");
            }

            public String FormatDay(Int32 days)
            {
                return Format(days, days == 1 ? "dzień" : "dni");
            }

            public String FormatMinute(Int32 minutes)
            {
                var m = minutes > 20 ? minutes % 10 : minutes;
                return Format(minutes, minutes == 1 ? "minuta" : m >= 2 && m <= 4 ? "minuty" : "minut");
            }

            public String FormatSecond(Int32 seconds)
            {
                var s = seconds % 10;
                return Format(seconds, seconds == 1 ? @"sekunda" : s >= 2 && s < 4 ? @"sekundy" : @"sekund");
            }

            public String GetConnector()
            {
                return "i";
            }

            private String Format(Int32 value, String suffix)
            {
                return $"{value:#,##0} {suffix}";
            }
        }

        [SuppressMessage("ReSharper", "InconsistentNaming")]
        internal class Localizer_enUS : ITimeSpanLocalizer
        {
            public String FormatHour(Int32 hours)
            {
                return Format(hours, "hour");
            }

            public String FormatYear(Int32 years)
            {
                return Format(years, "year");
            }

            public String FormatMonth(Int32 months)
            {
                return Format(months, "month");
            }

            public String FormatDay(Int32 days)
            {
                return Format(days, "day");
            }

            public String FormatMinute(Int32 minutes)
            {
                return Format(minutes, "minute");
            }

            public String FormatSecond(Int32 seconds)
            {
                return Format(seconds, "second");
            }

            public String GetConnector()
            {
                return "and";
            }

            private String Format(Int32 value, String suffix)
            {
                return $"{value:#,##0} {(value == 1 ? suffix : $"{suffix}s")}";
            }
        }

        public interface ITimeSpanLocalizer
        {
            String FormatHour(Int32 hours);

            String FormatYear(Int32 years);

            String FormatMonth(Int32 months);

            String FormatDay(Int32 days);

            String FormatMinute(Int32 minutes);

            String FormatSecond(Int32 seconds);

            String GetConnector();
        }

        private static Dictionary<String, ITimeSpanLocalizer> localizers;

        private static ITimeSpanLocalizer GetLocalizer(String culture)
        {
            if (localizers == null)
            {
                localizers = new Dictionary<String, ITimeSpanLocalizer>();
                localizers["pl-PL"] = new Localizer_plPL();
                localizers["en-US"] = new Localizer_enUS();
            }

            return localizers.ContainsKey(culture) ? localizers[culture] : localizers["en-US"];
        }

        public static String ToUserFriendlyString(this TimeSpan span, ITimeSpanLocalizer localizer)
        {
            const Double daysInYear = 365.2425;
            const Double daysInMonth = daysInYear / 12;

            // Get each non-zero value from TimeSpan component
            var values = new List<String>();

            // Number of years
            var days = span.Days;
            if (days >= daysInYear)
            {
                var years = (days / daysInYear);
                values.Add(localizer.FormatYear((Int32)years));
                days = (Int32)(days % daysInYear);
            }

            // Number of months
            if (days >= daysInMonth)
            {
                var months = (Int32)(days / daysInMonth);
                values.Add(localizer.FormatMonth(months));
                days = (Int32)(days % daysInMonth);
            }

            // Number of days
            if (days >= 1)
            {
                values.Add(localizer.FormatDay(days));
            }

            // Number of hours
            if (span.Hours >= 1)
            {
                values.Add(localizer.FormatHour(span.Hours));
            }

            // Number of minutes
            if (span.Minutes >= 1)
            {
                values.Add(localizer.FormatMinute(span.Minutes));
            }

            // Number of seconds (include when 0 if no other components included)
            if (span.Seconds >= 1 || values.Count == 0)
            {
                values.Add(localizer.FormatSecond(span.Seconds));
            }

            // Combine values into string
            var builder = new StringBuilder();
            for (var i = 0; i < values.Count; i++)
            {
                if (builder.Length > 0)
                {
                    builder.Append((i == values.Count - 1) ? $" {localizer.GetConnector()} " : ", ");
                }

                builder.Append(values[i]);
            }

            // Return result
            return builder.ToString();
        }

        /// <summary>
        ///     Constructs a user-friendly string for this TimeSpan instance.
        ///     <para>Code by Jonathan Wood from http://www.blackbeltcoder.com/Articles/time/creating-a-user-friendly-timespan-string.</para>
        ///     Original code with optional localization.
        /// </summary>
        public static String ToUserFriendlyString(this TimeSpan span, CultureInfo cultureInfo = null)
        {
            return ToUserFriendlyString(span, GetLocalizer(cultureInfo?.Name ?? CultureInfo.CurrentCulture.Name));
        }
    }
}