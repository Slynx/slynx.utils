﻿// 
// MethodInfoExtensions.cs is a part of Slynx.Utils project.
// 
// Created by Slynx on 10/05/2020 23:13.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Slynx.Utils.ExtensionMethods
{
    public static class MethodInfoExtensions
    {
        private static Dictionary<Int32, Type> Func { get; } = new Dictionary<Int32, Type>
        {
            { 0, typeof(Func<>) },
            { 1, typeof(Func<,>) },
            { 2, typeof(Func<,,>) },
            { 3, typeof(Func<,,,>) },
            { 4, typeof(Func<,,,,>) },
            { 5, typeof(Func<,,,,,>) },
            { 6, typeof(Func<,,,,,,>) },
            { 7, typeof(Func<,,,,,,,>) },
            { 8, typeof(Func<,,,,,,,,>) },
            { 9, typeof(Func<,,,,,,,,,>) },
            { 10, typeof(Func<,,,,,,,,,,>) },
            { 11, typeof(Func<,,,,,,,,,,,>) },
            { 12, typeof(Func<,,,,,,,,,,,,>) },
            { 13, typeof(Func<,,,,,,,,,,,,,>) },
            { 14, typeof(Func<,,,,,,,,,,,,,,>) },
            { 15, typeof(Func<,,,,,,,,,,,,,,,>) },
            { 16, typeof(Func<,,,,,,,,,,,,,,,,>) }
        };

        private static Dictionary<Int32, Type> Action { get; } = new Dictionary<Int32, Type>
        {
            { 0, typeof(Action) },
            { 1, typeof(Action<>) },
            { 2, typeof(Action<,>) },
            { 3, typeof(Action<,,>) },
            { 4, typeof(Action<,,,>) },
            { 5, typeof(Action<,,,,>) },
            { 6, typeof(Action<,,,,,>) },
            { 7, typeof(Action<,,,,,,>) },
            { 8, typeof(Action<,,,,,,,>) },
            { 9, typeof(Action<,,,,,,,,>) },
            { 10, typeof(Action<,,,,,,,,,>) },
            { 11, typeof(Action<,,,,,,,,,,>) },
            { 12, typeof(Action<,,,,,,,,,,,>) },
            { 13, typeof(Action<,,,,,,,,,,,,>) },
            { 14, typeof(Action<,,,,,,,,,,,,,>) },
            { 15, typeof(Action<,,,,,,,,,,,,,,>) },
            { 16, typeof(Action<,,,,,,,,,,,,,,,>) }
        };

        public static Boolean IsOverridden(this MethodInfo info)
        {
            return info.IsVirtual && info.GetBaseDefinition().DeclaringType != info.DeclaringType;
        }

        public static void InvokeNotOverriden(this MethodInfo info, Object instance, params Object[] pars)
        {
            InvokeNotOverriden(info, instance, pars.Select(s => (s, s.GetType())).ToArray());
        }

        public static void InvokeNotOverriden(this MethodInfo info, Object instance, (Object, Type)[] pars)
        {
            AssertNotRef(info);
            var args = pars.Select(s => s.Item1).ToArray();
            Type[] types = pars.Select(s => s.Item2 ?? s.Item1.GetType()).ToArray();

            if (!Action.ContainsKey(pars.Length))
            {
                throw new Exception($"Action delegate not found for {pars.Length} parameters");
            }

            Type type = pars.Length > 0 ? Action[pars.Length].MakeGenericType(types) : Action[pars.Length];
            ((Delegate)Activator.CreateInstance(type, instance, info.MethodHandle.GetFunctionPointer())).DynamicInvoke(args);
        }

        private static void AssertNotRef(MethodInfo info)
        {
            if (info.GetParameters().Any(s => s.ParameterType.IsByRef))
            {
                throw new NotSupportedException("Parameters of type 'ref' and 'out' are not supported");
            }
        }

        public static TResult InvokeNotOverriden<TResult>(this MethodInfo info, Object instance, (Object, Type)[] pars)
        {
            AssertNotRef(info);
            var args = pars.Select(s => s.Item1).ToArray();
            var array = new Type[pars.Length + 1];
            for (var i = 0; i < pars.Length; i++)
            {
                array[i] = pars[i].Item2 ?? pars[i].Item1.GetType();
            }

            array[array.Length - 1] = typeof(TResult);

            if (!Func.ContainsKey(pars.Length))
            {
                throw new Exception($"Func delegate not found for {pars.Length} parameters");
            }

            Type type = Func[pars.Length].MakeGenericType(array);
            return (TResult)((Delegate)Activator.CreateInstance(type, instance, info.MethodHandle.GetFunctionPointer())).DynamicInvoke(args);
        }

        public static TResult InvokeNotOverriden<TResult>(this MethodInfo info, Object instance, params Object[] pars)
        {
            return InvokeNotOverriden<TResult>(info, instance, pars.Select(s => (s, s.GetType())).ToArray());
        }
    }
}