﻿// 
// StreamExtensions.cs is a part of Slynx.Utils project.
// 
// Created by Slynx on 26/10/2020 01:16.

using System;
using System.IO;

namespace Slynx.Utils.ExtensionMethods
{
    /// <summary>
    ///     Extension methods for <see cref="Stream" />.
    /// </summary>
    public static class StreamExtensions
    {
        /// <summary>
        ///     Read all data from stream and returns as <see cref="Byte" /> array.
        /// </summary>
        /// <param name="stream"></param>
        /// <param name="resetPosition">Indicates if position in stream should be zeroed before reading</param>
        /// <returns></returns>
        public static Byte[] ReadBytes(this Stream stream, Boolean resetPosition = false)
        {
            if (stream is MemoryStream mse)
            {
                return mse.ToArray();
            }

            if (resetPosition)
            {
                stream.Position = 0;
            }

            using (var ms = new MemoryStream())
            {
                stream.CopyTo(ms);
                return ms.ToArray();
            }
        }
    }
}