﻿// 
// ObjectExtensions.cs is a part of Slynx.Utils project.
// 
// Created by Slynx on 31/12/2020 11:23.

using System;

namespace Slynx.Utils.ExtensionMethods
{
    public static class ObjectExtensions
    {
        /// <summary>
        ///     Converts object to dynamic and exposes all private members.
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public static dynamic AsExposed(this Object source)
        {
            if (source is Exposed)
            {
                return source;
            }

            if (source is null)
            {
                return null;
            }

            return new Exposed(source);
        }
    }
}