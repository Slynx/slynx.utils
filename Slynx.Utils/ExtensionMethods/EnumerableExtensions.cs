﻿// 
// EnumerableExtensions.cs is a part of Slynx.Utils project.
// 
// Created by Slynx on 23/10/2020 00:48.

using System;
using System.Collections.Generic;
using System.Linq;

namespace Slynx.Utils.ExtensionMethods
{
    /// <summary>
    ///     Extension methods for <see cref="IEnumerable{T}" />.
    /// </summary>
    public static class EnumerableExtensions
    {
        private static readonly Random random = new Random((Int32)DateTime.Now.Ticks);

        /// <summary>
        ///     Produces the set difference using the specified <see cref="Func{T,T,Boolean}" /> to compare values.
        /// </summary>
        /// <typeparam name="T">The type of the elements of the sequences.</typeparam>
        /// <typeparam name="TKey"></typeparam>
        /// <param name="enumerable">An <see cref="IEnumerable{T}" /> whose elements will be checked.</param>
        /// <param name="comparer">An <see cref="Func{T,T,Boolean}" /> to compare values.</param>
        /// <param name="selector">Key selector</param>
        /// <returns></returns>
        public static IEnumerable<T> DistinctBy<T, TKey>(this IEnumerable<T> enumerable, Func<T, TKey> selector, IEqualityComparer<TKey> comparer = null)
        {
            var set = new HashSet<TKey>(comparer);
            foreach (var element in enumerable)
            {
                if (set.Add(selector(element)))
                    yield return element;
            }
        }

        /// <summary>
        ///     Invokes <see cref="Action{T}" /> on each object in <see cref="IEnumerable{T}" />.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="enumerable"><see cref="IEnumerable{T}" /> to invoke on. Cannot be null.</param>
        /// <param name="action"><see cref="Action{T}" /> delegate to invoke on each object. Cannot be null.</param>
        public static void ForEach<T>(this IEnumerable<T> enumerable, Action<T> action)
        {
            foreach (var obj in enumerable)
            {
                action(obj);
            }
        }

        /// <summary>
        ///     Returns random element from IEnumerable.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="enumerable"></param>
        /// <exception cref="System.ArgumentNullException">Thrown when IEnumerable is null</exception>
        /// <exception cref="System.ArgumentOutOfRangeException">Thrown when IEnumerable is empty</exception>
        /// <returns></returns>
        public static T Draw<T>(this IEnumerable<T> enumerable)
        {
            T[] array = enumerable as T[] ?? enumerable.ToArray();
            return array.Length == 0 ? default : array[random.Next(0, array.Length)];
        }

        // Modified methods (MaxBy, MinBy) from the Jon Skeet's old code from MoreLinq project (2008)
        /// <summary>
        ///     Returns the maximal element of the given sequence, based on
        ///     the given projection.
        /// </summary>
        /// <remarks>
        ///     If more than one element has the maximal projected value, the first
        ///     one encountered will be returned. This operator uses immediate execution, but
        ///     only buffers a single result (the current maximal element).
        /// </remarks>
        /// <typeparam name="TSource">Type of the source sequence</typeparam>
        /// <typeparam name="TKey">Type of the projected element</typeparam>
        /// <param name="source">Source sequence</param>
        /// <param name="selector">Selector to use to pick the results to compare</param>
        /// <returns>The maximal element, according to the projection.</returns>
        public static TSource MaxBy<TSource, TKey>(this IEnumerable<TSource> source, Func<TSource, TKey> selector) where TKey : IComparable
        {
            using (IEnumerator<TSource> sourceIterator = source.GetEnumerator())
            {
                if (!sourceIterator.MoveNext())
                {
                    return default;
                }

                var max = sourceIterator.Current;
                var maxKey = selector(max);
                while (sourceIterator.MoveNext())
                {
                    var candidate = sourceIterator.Current;
                    var candidateProjected = selector(candidate);
                    if (candidateProjected.CompareTo(maxKey) > 0)
                    {
                        max = candidate;
                        maxKey = candidateProjected;
                    }
                }

                return max;
            }
        }

        /// <summary>
        ///     Returns the minimal element of the given sequence, based on
        ///     the given projection.
        /// </summary>
        /// <remarks>
        ///     If more than one element has the minimal projected value, the first
        ///     one encountered will be returned. This operator uses immediate execution, but
        ///     only buffers a single result (the current minimal element).
        /// </remarks>
        /// <typeparam name="TSource">Type of the source sequence</typeparam>
        /// <typeparam name="TKey">Type of the projected element</typeparam>
        /// <param name="source">Source sequence</param>
        /// <param name="selector">Selector to use to pick the results to compare</param>
        /// <returns>The minimal element, according to the projection.</returns>
        public static TSource MinBy<TSource, TKey>(this IEnumerable<TSource> source, Func<TSource, TKey> selector) where TKey : IComparable
        {
            using (IEnumerator<TSource> sourceIterator = source.GetEnumerator())
            {
                if (!sourceIterator.MoveNext())
                {
                    return default;
                }

                var min = sourceIterator.Current;
                var minKey = selector(min);
                while (sourceIterator.MoveNext())
                {
                    var candidate = sourceIterator.Current;
                    var candidateProjected = selector(candidate);
                    if (candidateProjected.CompareTo(minKey) < 0)
                    {
                        min = candidate;
                        minKey = candidateProjected;
                    }
                }

                return min;
            }
        }
    }
}