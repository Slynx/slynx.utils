﻿// 
// StringExtensions.cs is a part of Slynx.Utils project.
// 
// Created by Slynx on 10/05/2020 23:13.

using System;
using System.Linq;
using System.Text.RegularExpressions;
using Slynx.Utils.Core;

namespace Slynx.Utils.ExtensionMethods
{
    /// <summary>
    ///     Extension methods for <see cref="System.String" />
    /// </summary>
    public static class StringExtensions
    {
        /// <summary>
        ///     Returns substring between two other strings.
        /// </summary>
        /// <param name="str">Source element to work on</param>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <param name="includeStartEnd">Includes <paramref name="start" /> and <paramref name="end" /> strings in result</param>
        /// <returns></returns>
        public static String FindBetween(this String str, String start, String end, Boolean includeStartEnd = false)
        {
            try
            {
                var rgx = new Regex($"(?<={(String.IsNullOrWhiteSpace(start) ? "^" : Regex.Escape(start))})(?s).*?(?={(String.IsNullOrWhiteSpace(end) ? "$" : Regex.Escape(end))})");
                return includeStartEnd ? start + rgx.Match(str).Value + end : rgx.Match(str).Value;
            }
            catch
            {
                return String.Empty;
            }
        }

        /// <summary>
        ///     Works exactly as <see cref="String.Contains(string)" /> but takes multiple strings to check.
        ///     It's uses 'or' operator to check ("||").
        /// </summary>
        /// <param name="str">Source element to work on</param>
        /// <param name="strings">Strings to check</param>
        /// <returns></returns>
        /// <example>
        ///     if (str.Contains("a") || str.Contains("b") || str.Contains("c"))
        ///     // works in this same way as
        ///     if (str.ContainsAny("a", "b", "c"))
        /// </example>
        public static Boolean ContainsAny(this String str, params String[] strings)
        {
            return str != null && strings.Any(str.Contains);
        }

        /// <summary>
        ///     Works exactly as <see cref="String.Contains(string)" /> but takes multiple strings to check.
        ///     It's uses 'and' operator to check ("&amp;&amp;").
        /// </summary>
        /// <param name="str">Source element to work on</param>
        /// <param name="strings">Strings to check</param>
        /// <returns></returns>
        /// <example>
        ///     if (str.Contains("a") &amp;&amp; str.Contains("b") &amp;&amp; str.Contains("c"))
        ///     // works in this same way as
        ///     if (str.ContainsAll("a", "b", "c"))
        /// </example>
        public static Boolean ContainsAll(this String str, params String[] strings)
        {
            return str != null && strings.All(str.Contains);
        }

        /// <summary>
        ///     Returns string limited to first <paramref name="count" /> characters (or it's maximum length) starting from left
        ///     side.
        /// </summary>
        /// <param name="str">Source element to work on</param>
        /// <param name="count">Characters count, e.g. 6 to take first six characters</param>
        /// <returns>
        ///     <see cref="String" /> limited to <paramref name="count" /> characters (or it's maximum length) or
        ///     <see cref="String.Empty" /> if it's 0 length or <c>null</c>
        /// </returns>
        public static String Left(this String str, Int32 count)
        {
            if (String.IsNullOrWhiteSpace(str))
            {
                return String.Empty;
            }

            return str.Substring(0, MathUtils.Clamp(count, 0, str.Length));
        }

        /// <summary>
        ///     Returns string limited to first <paramref name="count" /> characters (or it's maximum length) starting from right
        ///     side.
        /// </summary>
        /// <param name="str">Source element to work on</param>
        /// <param name="count">Characters count, e.g. 6 to take last six characters</param>
        /// <returns>
        ///     <see cref="String" /> limited to <paramref name="count" /> characters (or it's maximum length) or
        ///     <see cref="String.Empty" /> if it's 0 length or <c>null</c>
        /// </returns>
        public static String Right(this String str, Int32 count)
        {
            if (String.IsNullOrWhiteSpace(str))
            {
                return String.Empty;
            }

            return str.Substring(MathUtils.Clamp(str.Length - count, 0, str.Length), MathUtils.Clamp(count, 0, str.Length));
        }

        /// <summary>
        ///     Returns a new string that centers characters in this instance by padding them on the left and right with a
        ///     specified Unicode character,
        ///     for a specified total length (half of width from each side).
        /// </summary>
        /// <param name="str">Source element to work on</param>
        /// <param name="totalWidth">
        ///     The number of characters in the resulting string, equal to the number of original characters
        ///     plus any additional padding characters
        /// </param>
        /// <param name="paddingChar">A Unicode padding character</param>
        /// <returns></returns>
        public static String PadCenter(this String str, Int32 totalWidth, Char paddingChar)
        {
            if (str == null || totalWidth <= str.Length)
            {
                return str;
            }

            var padding = totalWidth - str.Length;
            return str.PadLeft(str.Length + padding / 2, paddingChar).PadRight(totalWidth, paddingChar);
        }

        /// <summary>
        ///     Returns a copy of this <see cref="string" /> with first character converted to uppercase
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static String ToUpperFirst(this String str)
        {
            if (String.IsNullOrWhiteSpace(str))
            {
                return String.Empty;
            }

            var chars = str.ToCharArray();
            chars[0] = Char.ToUpper(chars[0]);
            return new String(chars);
        }

        /// <summary>
        ///     Removes <paramref name="prefix" /> from the current <see cref="String" /> object, if exists.
        /// </summary>
        /// <param name="str"></param>
        /// <param name="prefix"></param>
        /// <returns></returns>
        public static String TrimPrefix(this String str, String prefix)
        {
            return str.StartsWith(prefix) ? str.Substring(prefix.Length) : str;
        }

        /// <summary>
        ///     Removes <paramref name="suffix" /> from current <see cref="String" /> object, if exists.
        /// </summary>
        /// <param name="str"></param>
        /// <param name="suffix"></param>
        /// <returns></returns>
        public static String TrimSuffix(this String str, String suffix)
        {
            return str.EndsWith(suffix) ? str.Substring(0, str.Length - suffix.Length) : str;
        }

        /// <summary>
        ///     Separates <see cref="String" /> value previously flattened using <see cref="EnumerableExtensions.Flatten" />
        ///     method.
        /// </summary>
        /// <param name="str"></param>
        /// <param name="separator"></param>
        /// <returns></returns>
        public static String[] Separate(this String str, String separator)
        {
            if (String.IsNullOrWhiteSpace(str))
            {
                return new String[0];
            }

            return str.Split(new[] { separator }, StringSplitOptions.RemoveEmptyEntries);
        }

        /// <summary>
        ///     Replaces strings in given string with last element of <paramref name="strings" /> array.
        /// </summary>
        /// <param name="str"></param>
        /// <param name="strings"></param>
        /// <returns></returns>
        public static String ReplaceWithLast(this String str, params String[] strings)
        {
            if (str == null)
            {
                return null;
            }

            for (var i = 0; i < strings.Length - 1; i++)
            {
                str = str.Replace(strings[i], strings[strings.Length - 1]);
            }

            return str;
        }

        /// <summary>
        ///     Replaces strings in given string by pairing elements in <paramref name="strings" /> array, e.g.
        ///     <code>
        ///     ReplaceByPair("s1s2s3s4", "s1", "a1", "s3", "a3")
        /// </code>
        ///     will result with "a1s2a3s4".
        /// </summary>
        /// <param name="str"></param>
        /// <param name="strings"></param>
        /// <returns></returns>
        public static String ReplaceByPair(this String str, params String[] strings)
        {
            if (str == null)
            {
                return null;
            }

            if (strings.Length % 2 != 0)
            {
                throw new ArgumentException("Number of strings must be even");
            }

            for (var i = 0; i < strings.Length; i += 2)
            {
                str = str.Replace(strings[i], strings[i + 1]);
            }

            return str;
        }
    }
}