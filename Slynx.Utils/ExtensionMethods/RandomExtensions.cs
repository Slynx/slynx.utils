﻿// 
// RandomExtensions.cs is a part of Slynx.Utils project.
// 
// Created by Slynx on 23/10/2020 01:52.

using System;
using System.Diagnostics;

namespace Slynx.Utils.ExtensionMethods
{
    /// <summary>
    ///     Extension methods for <see cref="System.Random" />
    /// </summary>
    public static class RandomExtensions
    {
        /// <summary>
        ///     Returns true or false depending on drawing chance. Tries to draw a number with given percentage chance, e.g.
        ///     Passing 90 as argument gives you a 90% chance to return true and 10% to return false.
        /// </summary>
        /// <param name="random"></param>
        /// <param name="percentageChance"></param>
        /// <returns></returns>
        public static Boolean Draw(this Random random, Int32 percentageChance)
        {
            Debug.Assert(percentageChance >= 1 && percentageChance <= 100);
            var num = random.Next(1, 101);
            return num >= 1 && num <= percentageChance;
        }
    }
}