﻿// 
// ImageExtensions.cs is a part of Slynx.Utils project.
// 
// Created by Slynx on 10/05/2020 23:13.

using System;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace Slynx.Utils.ExtensionMethods
{
    /// <summary>
    ///     Extension methods for <see cref="Image" />.
    /// </summary>
    public static class ImageExtensions
    {
        /// <summary>
        ///     Performs high quality resize on <see cref="Image" />.
        ///     Few important settings included in resizing are:
        ///     <see cref="CompositingQuality.HighQuality" />,
        ///     <see cref="InterpolationMode.HighQualityBicubic" />,
        ///     <see cref="SmoothingMode.HighQuality" />
        /// </summary>
        /// <param name="image">Original image to resize. It's not changed</param>
        /// <param name="width">New width in pixels</param>
        /// <param name="height">New height in pixels</param>
        /// <returns>Resized <see cref="Bitmap" /> object.</returns>
        public static Bitmap ResizeImage(this Image image, Int32 width, Int32 height)
        {
            var result = new Bitmap(width, height);
            //set the resolutions the same to avoid cropping due to resolution differences
            result.SetResolution(image.HorizontalResolution, image.VerticalResolution);

            using (var graphics = Graphics.FromImage(result))
            {
                //set the resize quality modes to high quality
                graphics.CompositingQuality = CompositingQuality.HighQuality;
                graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphics.SmoothingMode = SmoothingMode.HighQuality;
                //draw the image into the target bitmap
                graphics.DrawImage(image, 0, 0, result.Width, result.Height);
            }

            return result;
        }
    }
}