﻿// 
// ArrayExtensions.cs is a part of Slynx.Utils project.
// 
// Created by Slynx on 23/12/2020 00:10.

using System;
using Slynx.Utils.Core;

namespace Slynx.Utils.ExtensionMethods
{
    /// <summary>
    ///     Extension methods for arrays.
    /// </summary>
    public static class ArrayExtensions
    {
        /// <summary>
        ///     Creates a copy and inserts item to array into specified position. If position is outside array, item is added to the end or beginning of array.
        /// </summary>
        /// <param name="original"></param>
        /// <param name="item"></param>
        /// <param name="index"></param>
        /// <returns></returns>
        public static T[] Insert<T>(this T[] original, T item, Int32 index)
        {
            var result = new T[original.Length + 1];
            var i = 0;
            var correctIndex = MathUtils.Clamp(index, 0, result.Length - 1);

            while (i != correctIndex)
            {
                result[i] = original[i];
                i++;
            }

            result[i] = item;
            i++;
            while (i < result.Length)
            {
                result[i] = original[i - 1];
                i++;
            }

            return result;
        }
    }
}