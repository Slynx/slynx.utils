﻿// 
// Range.cs is a part of Slynx.Utils project.
// 
// Created by Slynx on 20/05/2020 23:16.

using System;

namespace Slynx.Utils
{
    /// <summary>
    ///     Simple structure to represent range between minimum and maximum value.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public struct Range<T> where T : IComparable<T>
    {
        /// <summary>
        ///     Constructor with minimum and maximum parameters.
        /// </summary>
        /// <param name="minimum">Minimum value in <see cref="Range{T}" /></param>
        /// <param name="maximum">Maximum value in <see cref="Range{T}" /></param>
        public Range(T minimum, T maximum) : this()
        {
            if (minimum.CompareTo(maximum) > 0)
            {
                throw new ArgumentException("maximum must be greater than minimum");
            }

            Minimum = minimum;
            Maximum = maximum;
        }

        /// <summary>
        ///     Minimum value of the <see cref="Range{T}" />
        /// </summary>
        public T Minimum { get; set; }

        /// <summary>
        ///     Maximum value of the <see cref="Range{T}" />
        /// </summary>
        public T Maximum { get; set; }

        /// <summary>
        ///     Presents the <see cref="Range{T}" /> in readable format
        /// </summary>
        /// <returns>String representation of the Range</returns>
        public override String ToString()
        {
            return $"[{Minimum} - {Maximum}]";
        }

        /// <summary>
        ///     Determines if the <see cref="Range{T}" /> is valid.
        /// </summary>
        /// <returns>True if range is valid, else false</returns>
        public Boolean IsValid()
        {
            return Minimum.CompareTo(Maximum) <= 0;
        }

        /// <summary>
        ///     Determines if the provided value is inside the <see cref="Range{T}" />.
        /// </summary>
        /// <param name="value">The value to test</param>
        /// <returns>True if the value is inside Range, else false</returns>
        public Boolean ContainsValue(T value)
        {
            return (Minimum.CompareTo(value) <= 0) && (value.CompareTo(Maximum) <= 0);
        }

        /// <summary>
        ///     Determines if this <see cref="Range{T}" /> is inside the bounds of another range
        /// </summary>
        /// <param name="range">The parent <see cref="Range{T}" /> to test on</param>
        /// <returns>True if range is inclusive, else false</returns>
        public Boolean IsInsideRange(Range<T> range)
        {
            return IsValid() && range.IsValid() && range.ContainsValue(Minimum) && range.ContainsValue(Maximum);
        }

        /// <summary>
        ///     Determines if another <see cref="Range{T}" /> is inside the bounds of this <see cref="Range{T}" />
        /// </summary>
        /// <param name="range">The child <see cref="Range{T}" /> to test</param>
        /// <returns>True if range is inside, else false</returns>
        public Boolean ContainsRange(Range<T> range)
        {
            return IsValid() && range.IsValid() && ContainsValue(range.Minimum) && ContainsValue(range.Maximum);
        }
    }
}