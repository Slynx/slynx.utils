// 
// BooleanResult.cs is a part of Slynx.Utils project.
// 
// Created by Slynx on 10/05/2020 23:13.

using System;

namespace Slynx.Utils
{
    /// <summary>
    ///     Represents simple result of <see cref="bool" /> type, attached <see cref="string" /> message and optional
    ///     <see cref="BooleanResult.Tag" /> object.
    ///     Object is implicitly converted to <see cref="bool" /> value.
    /// </summary>
    public struct BooleanResult
    {
        /// <summary>
        ///     Message associated with current result.
        /// </summary>
        public String Message { get; set; }

        /// <summary>
        ///     Result value - true or false.
        /// </summary>
        public Boolean Result { get; set; }

        /// <summary>
        ///     Optional Tag of <see cref="Object" /> type.
        /// </summary>
        public Object Tag { get; set; }

        /// <summary>
        ///     Implicit conversion from <see cref="BooleanResult" /> to <see cref="Boolean" />.
        /// </summary>
        /// <param name="result"></param>
        /// <returns></returns>
        public static implicit operator Boolean(BooleanResult result)
        {
            return result.Result;
        }

        /// <summary>
        ///     Implicit conversion from <see cref="Boolean" /> to <see cref="BooleanResult" />.
        ///     New object is created with empty <see cref="Message" /> and <c>null</c> <see cref="Tag" />.
        /// </summary>
        /// <param name="d"></param>
        /// <returns></returns>
        public static implicit operator BooleanResult(Boolean d)
        {
            return new BooleanResult { Result = d };
        }
    }
}