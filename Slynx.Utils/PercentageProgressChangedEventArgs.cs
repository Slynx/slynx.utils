﻿// 
// PercentageProgressChangedEventArgs.cs is a part of Slynx.Utils project.
// 
// Created by Slynx on 10/05/2020 23:13.

using System;
using System.Diagnostics;

namespace Slynx.Utils
{
    /// <summary>
    ///     Global <see cref="EventArgs" /> for all <see cref="EventHandler" />'s using percentage values.
    /// </summary>
    public class PercentageProgressChangedEventArgs : EventArgs
    {
        /// <summary>
        ///     Default constructor.
        /// </summary>
        /// <param name="value"></param>
        public PercentageProgressChangedEventArgs(Int32 value)
        {
            Debug.Assert(value >= 0 && value <= 100);
            Value = value;
        }

        /// <summary>
        ///     Constructor to calculate percentage progress from current and total value.
        /// </summary>
        /// <param name="current"></param>
        /// <param name="total"></param>
        public PercentageProgressChangedEventArgs(Double current, Double total)
        {
            Value = (Int32) ((current / total) * 100);
        }

        /// <summary>
        ///     Optional object associated with this <see cref="PercentageProgressChangedEventArgs" /> object.
        /// </summary>
        public Object Tag { get; set; }

        /// <summary>
        ///     Current percentage (0-100) progress.
        /// </summary>
        public Int32 Value { get; }
    }
}