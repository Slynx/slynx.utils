﻿// 
// IObjectResolver.cs is a part of Slynx.Utils project.
// 
// Created by Slynx on 10/05/2020 23:13.

namespace Slynx.Utils
{
    /// <summary>
    ///     Represents mechanism to transform (resolve) one object to the other.
    ///     This is very general pattern and can be used for many purposes.
    /// </summary>
    /// <typeparam name="TInput"></typeparam>
    /// <typeparam name="TOutput"></typeparam>
    public interface IObjectResolver<in TInput, out TOutput>
    {
        /// <summary>
        ///     Transforms <paramref name="input" /> object of <see cref="T:TInput" /> type to object of <see cref="T:TOutput" />
        ///     type.
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        TOutput Resolve(TInput input);
    }
}