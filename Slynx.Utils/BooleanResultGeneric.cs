﻿// 
// BooleanResultGeneric.cs is a part of Slynx.Utils project.
// 
// Created by Slynx on 10/05/2020 23:13.

using System;

namespace Slynx.Utils
{
    public class BooleanResult<T>
    {
        /// <summary>
        ///     Message associated with current result.
        /// </summary>
        public String Message { get; set; }

        /// <summary>
        ///     Result value - true or false.
        /// </summary>
        public Boolean Result { get; set; }

        /// <summary>
        ///     Optional Tag of <see cref="T" /> type.
        /// </summary>
        public T Tag { get; set; }

        /// <summary>
        ///     Implicit conversion from <see cref="BooleanResult{T}" /> to <see cref="Boolean" />.
        /// </summary>
        /// <param name="result"></param>
        /// <returns></returns>
        public static implicit operator Boolean(BooleanResult<T> result)
        {
            return result.Result;
        }

        /// <summary>
        ///     Implicit conversion from <see cref="Boolean" /> to <see cref="BooleanResult{T}" />.
        ///     New object is created with empty <see cref="Message" /> and <c>null</c> <see cref="Tag" />.
        /// </summary>
        /// <param name="d"></param>
        /// <returns></returns>
        public static implicit operator BooleanResult<T>(Boolean d)
        {
            return new BooleanResult<T> { Result = d };
        }
    }
}