﻿// 
// SlynxFormatProvider.cs is a part of Slynx.Utils project.
// 
// Created by Slynx on 10/05/2020 23:13.

using System;
using System.Globalization;

namespace Slynx.Utils
{
    /// <summary>
    ///     <see cref="IFormatProvider" /> specific implementation for size, size ranges, time, etc. used by all applications designed by Slynx.
    ///     Detailed list of supported formats:
    ///     <list type="bullet">
    ///         <item>
    ///             <term>
    ///                 FS - format file size, eg. <see cref="String.Format(IFormatProvider, String, Object)" /> (formatter, "{0:FS}", 1907341823) => 1,78GB, where number must be of type
    ///                 convertible to <see cref="Double" />.
    ///             </term>
    ///         </item>
    ///         <item>
    ///             <term>
    ///                 FSR - format file size range, eg. <see cref="String.Format(IFormatProvider, String, Object)" /> (formatter, "{0:FSR}", range) => 120,54KB-4,35MB, where range is of
    ///                 <see cref="Range{Double}" /> type.
    ///             </term>
    ///         </item>
    ///         <item>
    ///             <term>
    ///                 SPEED - format processing speed, eg. <see cref="String.Format(IFormatProvider, String, Object)" /> (formatter, "{0:SPEED}", 3242342) => 3,09MB/s, where number must
    ///                 be of type convertible to
    ///                 <see cref="Double" />.
    ///             </term>
    ///         </item>
    ///         <item>
    ///             <term>
    ///                 TIME - format time, eg. <see cref="String.Format(IFormatProvider, String, Object)" /> (formatter, "{0:TIME}", 2334) => 00:38:54, where number must be of type
    ///                 convertible to <see cref="Int32" />.
    ///             </term>
    ///         </item>
    ///     </list>
    /// </summary>
    public class SlynxFormatProvider : IFormatProvider, ICustomFormatter
    {
        /// <summary>
        ///     Suffixes for sizes.
        /// </summary>
        private static readonly String[] orderedSuffixes = { "B", "KB", "MB", "GB", "TB", "PB", "EB" };

        /// <summary>
        ///     Converts the value of a specified object to an equivalent string representation using specified format and
        ///     culture-specific formatting information.
        /// </summary>
        /// <returns>
        ///     The string representation of the value of <paramref name="arg" />, formatted as specified by
        ///     <paramref name="format" /> and <paramref name="formatProvider" />.
        /// </returns>
        /// <param name="format">A format string containing formatting specifications. </param>
        /// <param name="arg">An object to format. </param>
        /// <param name="formatProvider">An object that supplies format information about the current instance. </param>
        public String Format(String format, Object arg, IFormatProvider formatProvider)
        {
            switch (format.ToUpper())
            {
                case "FS":
                {
                    return FormatSize(Convert.ToDouble(arg));
                }
                case "FSR":
                {
                    return FormatSizeRange((Range<Double>)arg);
                }
                case "SPEED":
                {
                    return FormatSpeed(Convert.ToDouble(arg));
                }
                case "TIME":
                {
                    return FormatTime(Convert.ToInt32(arg));
                }
                default:
                {
                    return HandleOtherFormats(format, arg);
                }
            }
        }

        /// <summary>
        ///     Returns an object that provides formatting services for the specified type.
        /// </summary>
        /// <returns>
        ///     An instance of the object specified by <paramref name="formatType" />, if the
        ///     <see cref="T:System.IFormatProvider" /> implementation can supply that type of object; otherwise, null.
        /// </returns>
        /// <param name="formatType">An object that specifies the type of format object to return. </param>
        public Object GetFormat(Type formatType)
        {
            return this;
        }

        /// <summary>
        ///     Format speed in bytes per second as string representation.
        /// </summary>
        /// <example><paramref name="speed" />(1024) -> 1KB/s</example>
        /// <param name="speed">Speed in bytes per second</param>
        /// <returns>Formatted <see cref="String" /></returns>
        private static String FormatSpeed(Double speed)
        {
            if (speed <= 0)
            {
                return String.Empty;
            }

            var suffix = 0;
            while (speed >= 1024)
            {
                speed /= 1024;
                suffix++;
            }

            return $"{speed:0.00} {orderedSuffixes[suffix]}/s";
        }

        /// <summary>
        ///     Format Time from seconds. If seconds is less than 0, <see cref="String.Empty" /> is returned.
        /// </summary>
        /// <param name="seconds">Time in seconds</param>
        /// <returns>Formatted <see cref="String" /> or <see cref="String.Empty" /> if <paramref name="seconds" /> is less than 0</returns>
        private static String FormatTime(Int32 seconds)
        {
            if (seconds < 0)
            {
                return String.Empty;
            }

            var ts = TimeSpan.FromSeconds(seconds);
            return $"{ts.Hours:D2}:{ts.Minutes:D2}:{ts.Seconds:D2}";
        }

        /// <summary>
        ///     Formats size range (from n to m) using <see cref="Range{Int32}" /> as input.
        ///     <see cref="Range{Int32}.Minimum" /> and <see cref="Range{Int32}.Maximum" /> can contains this
        ///     same value and formatting should be redirected to <see cref="FormatSize" />
        /// </summary>
        /// <param name="size">Size range to format represented as <see cref="Range{T}" /></param>
        /// <returns>Formatted <see cref="String" />, e.g. '10KB-20KB'</returns>
        private static String FormatSizeRange(Range<Double> size)
        {
            if (Math.Abs(size.Minimum - size.Maximum) < 0.0001)
            {
                return FormatSize(size.Minimum);
            }

            return FormatSize(size.Minimum) + "-" + FormatSize(size.Maximum);
        }

        /// <summary>
        ///     Format size in bytes as <see cref="String" /> representation.
        /// </summary>
        /// <example>
        ///     <paramref name="size" />(1024) -> 1KB, <paramref name="size" />(0x400000000) ->
        ///     1GB, <paramref name="size" />(1026) -> 1.02KB
        /// </example>
        /// <param name="size">Size in bytes</param>
        /// <returns>Formatted <see cref="String" /></returns>
        private static String FormatSize(Double size)
        {
            var suffix = 0;
            while (size >= 1024)
            {
                size /= 1024;
                suffix++;
            }

            return $"{size:N2} {orderedSuffixes[suffix]}";
        }

        private static String HandleOtherFormats(String format, Object arg)
        {
            if (arg is IFormattable formattable)
            {
                return formattable.ToString(format, CultureInfo.CurrentCulture);
            }

            return arg != null ? arg.ToString() : String.Empty;
        }
    }
}